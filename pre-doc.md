# PST BOT TWITCH DOCUMENTATION

### Command: `!command_name`

**Description:**
> Briefly describe what the command does.  
> Example: This command displays the current uptime of the stream.

**Usage:**
> Explain how viewers or mods should use the command.  
> Example: `!uptime`

**Permissions:**
> Indicate who can use the command (e.g., Everyone, Mods, VIPs, Broadcaster).  
> Example: `Everyone`

**Cooldown:**
> Specify the cooldown time, if applicable. This is the time users must wait before they can use the command again.  
> Example: `10 seconds`

**Response:**
> Provide the exact response the bot will give when the command is used.  
> Example: `The stream has been live for {uptime}`

**Aliases:**
> List any alternate commands that trigger the same response, if applicable.  
> Example: `!live, !howlong`

**Variables Used:**
> List any variables used in the response that pull in dynamic data (e.g., `{uptime}`, `{username}`, `{count}`).  
> Example: `{uptime} - Shows how long the stream has been live.`

**Examples:**
> Provide one or more examples showing the command in action, including the input and the bot’s response.  
> Example:  
> - **User Input:** `!uptime`  
> - **Bot Response:** `The stream has been live for 2 hours, 15 minutes.`