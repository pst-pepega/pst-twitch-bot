import math
import json
from urllib.request import urlopen 
import sys

def calc(race, lfm_name, position):
	url = ("https://api2.lowfuelmotorsport.com/api/race/" + str(race))
	return_string = ""
	basic_info = ""
	car_class = ""

  
	response = urlopen(url)
	data = json.loads(response.read())

	basic_info += f"\nRace at {data['track']['track_name']} at {data['race_date']}"

	# drivers = [5368, 5018, 4922, 4726, 4697, 4319, 4170, 3799, 3633, 3624, 3409, 3322, 3092, 3060, 2913, 2757, 2735, 2701, 2611, 2516, 2372, 2368, 2358, 2299, 2290, 2277, 2257, 2255, 2249, 2232, 2226, 2190, 2168, 2102, 2097]
	drivers = []
	driver = 0

	for x in data["participants"]["entries"]:
		if f'{x["vorname"]} {x["nachname"]}' == lfm_name:
			car_class = x['car_class']

	for x in data["participants"]["entries"]:
		if car_class == x['car_class']:
			drivers.append(x["elo"])
		if f'{x["vorname"]} {x["nachname"]}' == lfm_name:
			driver = x["elo"]
			basic_info += f"\nYour user is {x['vorname']} {x['nachname']} with {x['elo']} elo. [{car_class}]"
	
	rBase = 1600 / math.log(2)
	elogain = 0
	k_factor =  float(data["event"]["k_factor"])
	drivers_count = len(drivers)

	basic_info += "\n" +  ("K-factor: " + str(k_factor))

	for y in range(1, drivers_count+1):	
		elogain = 0
		fudgeFactor = ((drivers_count - 0) / 2 - y) / 100	
		for x in drivers:
			if (x != driver):
				a = (1 - math.exp(-driver / rBase)) * math.exp(-x / rBase)
				b = (1 - math.exp(-x / rBase)) * math.exp(-driver / rBase)
				c = (1 - math.exp(-driver / rBase)) * math.exp(-x / rBase)
				score = a / (b + c)
				#score = (drivers_count - driver_pos - score - fudgeFactor) * 200 / drivers_count * k_factor
				# print(str(score))
				elogain = elogain + score
		
		elogain = (drivers_count - y - elogain - fudgeFactor) * 200 / drivers_count * k_factor
		#print(abs(elogain))
		#print(y)		

		if y == position:
			return_string += f" — For Position {y}, you gain {round(elogain, 4)}"

		print(f"{basic_info} — {return_string}" )
		return f"{basic_info} — {return_string}" 

#print(calc(99992, "Teemu Karppinen", 10))