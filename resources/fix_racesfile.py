import pandas as pd

# Load the CSV file
df = pd.read_csv("races.csv")

# Drop duplicates based on the 'id' column, keeping the first occurrence
df = df.drop_duplicates(subset=["id"], keep="first")

# Save the cleaned CSV
df.to_csv("races.csv", index=False)
