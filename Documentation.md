
# PST BOT TWITCH DOCUMENTATION

### Command: `!fuel`

**Description:**
Calculates fuel for a race or similar.

**Usage:**
`!fuel [laptime: 1:42.444 or 1:42.44 or 1:42.4] [duration in minutes] [fuel per lap]` Order irrelevant

**Response:**
`$FUEL [$LAPS Laps]`  
Second message explains further how much fuel for a formation lap and how close it is to another lap.

**Variables Used:**
`$FUEL - Shows the fuel needed.`  
`$LAPS - Shows the laps for the race.`

**Examples:**
- **User Input:** `!fuel 30 2:12 3.1`  
- **Bot Response:** `43.4 [14 Laps]`
- **Bot Response:** `For a 30.0 minute race with a 2:12.000 lap time ─ Total Fuel: 43.4 liters ─ Fuel (incl. formation lap): 48.05 liters ─ Bonus Lap Margin: -48.0 seconds (excluding pit stops)`

______

### Command: `!mostactive`

**Description:**
Displays where the streamer lies within the most active users of LFM, or another driver per lfm-id or name or check a position in that list.

**Usage:**
`!mostactive`   
`!mostactive $NAME`   
`!mostactive id $LFM-ID`   
`!mostactive pos $POSITION`   

**Response:**
`[position]. [driver-name] [information].`

**Variables Used:**
`$NAME - LFM Name`
`$LFM-ID - lfm-id`
`$POSITION - Position in the LFM Top 100 most active user list`

**Examples:**
- **User Input:** `!mostactive`  
- **Bot Response:** `55. Emir al-Batee' ─ 1007 races (Δ147) ─ 23422 laps (Δ415) ─ 120502.8630 kilometers (Δ15.321)`

- **User Input:** `!mostactive id 2`  
- **Bot Response:** `99265.2170 kilometres until you are in the top 100 most active users on LFM. [And 19396 laps.]`

- **User Input:** `!mostactive Boris Schalk`  
- **Bot Response:** `12. Boris Schalk ─ 1303 races (Δ556) ─ 35758 laps (Δ2650) ─ 187321.8880 kilometers (Δ2986.608)`

- **User Input:** `!mostactive pos 12`  
- **Bot Response:** `12. Boris Schalk ─ 1303 races (Δ556) ─ 35758 laps (Δ2650) ─ 187321.8880 kilometers (Δ2986.608)`

______

### Command: `!steerratio`

**Description:**
Displays the steer ratio for various cars.

**Usage:**
`!steerratio [search-query]`

**Response:**
`{'Brand': '$BRAND', 'Model': '$MODEL', 'Steering lock': '$STEERRATIO'}`  

**Variables Used:**
`$BRAND - The brand of car`  
`$MODEL - A specific model for the brand.`
`$STEERRATIO - Shows the maximum steer ratio for a specific car.`

**Examples:**
- **User Input:** `!steerratio Ferrari`  
- **Bot Response:** `{'Brand': 'Ferrari', 'Model': '488 GT3', 'Steering lock': '480°'}`
- **Bot Response:** `{'Brand': 'Ferrari', 'Model': '488 GT3 EVO', 'Steering lock': '480°'}`
- **Bot Response:** `{'Brand': 'Ferrari', 'Model': '296 GT3', 'Steering lock': '800°'}`

______

### Command: `!lastrace`

**Description:**
Displays the last race result for the streamer.

**Usage:**
`!lastrace`

**Response:**
`Latest LFM race for $NAME ─ $INFOS`  

**Variables Used:**
`$NAME - Name of the driver`  
`$INFOS - Race result infos about the race.`

**Examples:**
- **User Input:** `!lastrace`  
- **Bot Response:** `Latest LFM race for George Boothby ─ 📉 P2 -> P4 [-2] ─ 📉 -19 Elo ─ 📈 0.31 SR [3 IP]`

______

### Command: `!pit`

______

### Command: `!wr`

**OUTDATED?**

______

### Command: `!race`

**Description:**
Displays various information about an LFM race.

______

### Command: `!stocks`

**Description:**
Displays current stock price for a stock and changes.

______

### Command: `!lfm`

**Description:**
Displays in which series a certain track is present in the LFM Calendar.

______

### Command: `!nextweek`

**Description:**
Displays the next week's schedule on LFM.

______

### Command: `!currentweek`

**Description:**
Displays the current week's schedule on LFM.

______

### Command: `!thisweek`

**Description:**
Works the same as !currentweek

______

### Command: `!teemu`

**Description:**
Displays Teemu Karppinen's popometer link.

______

### Command: `!lfmhl`

______

### Command: `!adduser`

**Description:**
Adds the bot temporarily to your channel's chat.

______

### Command: `!limit`

______

### Command: `!commands`

**Description:**
Displays popular (or all) commands.

______

### Command: `!discordbot`

**Description:**
Displays the invite link for the discord bot.

______

### Command: `!bop`

______

### Command: `!custombop`

**Description:**
Creates a custom bop for the user.

______

### Command: `!realbop`

______

### Command: `!entrylist`

______

### Command: `!proseries`

**Description:**
Displays various information for the next (or any other) Pro Series race.

______

### Command: `!laprecords`

**Description:**
Displays the streamers' lap records on LFM for a given track.

______

### Command: `!gt4laprecords`

**Description:**
Displays the streamers' GT4 lap records on LFM for a given track.

______

### Command: `!gt2laprecords`

**Description:**
Displays the streamers' GT2 lap records on LFM for a given track.

______

### Command: `!stats`

**Description:**
Displays various LFM statistics for the streamer.

______

### Command: `!target`

**Description:**
Displays the BOP target time for a track.

______

### Command: `!bopcar`

**Description:**
Displays the BOP information for a car and track combination.

______

### Command: `!elocalc`

**Description:**
Calculates the elo for the streamer or any driver in a race based on the finishing position

______

### Command: `!penaltypoints`

______

### Command: `!search`

______

### Command: `!weathercalc`

______

### Command: `!nords`

______

### Command: `!accdown`

______

### Command: `!accstatus`

**Description:**
Works the same as !accdown

______

### Command: `!mph`

______

### Command: `!kmh`

______

### Command: `!livetiming`

______

### Command: `!leaderboard`

______

### Command: `!lfmlaprecords`

______

### Command: `!weather`

______

### Command: `!accdown`

______

### Command: `!offseason`
