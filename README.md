# PST scripts

The place where the ultimate 5HEAD write scripts that have no purpose :)

## pst-lfm script usage

Install requirements

```sh
pip install -r requirements.txt
```

Create the config file `config.json`

```
{
    "discordToken": "<token>",
    "users": [
        "id": 12345,
        "name": "John Wick"
    ]
}
```

Run the script

```sh
python pst-lfm.py
```

### Optional: Install the timer

You can use the Systemd timer to run the script at a set interval

```sh
sudo cp pst-lfm.service pst-lfm.timer /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable --now pst-lfm.timer
```


## youtube scheduler usage

Install requirements [maybe ryan will make a file]

You need a youtube api key (in youtube_key.json) and a calendar api key. (in key.json)  
Need to specify the google calendar id in line 27.   
events.json stores the events from the calendar, to not schedule them more than once.   
On line 127 a thumbnail folder can/must be specified.   
description.txt has the description for the youtube live stream description   

How to run:
```sh
py run.py
```
After running, need to login to a google account where the live stream should be scheduled.

