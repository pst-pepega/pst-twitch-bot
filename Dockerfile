#### BUILD ####

FROM python:3.11.11 as build
WORKDIR /build 

RUN pip install 'poetry~=2.0.0'

ENV POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_IN_PROJECT=1 \
    POETRY_VIRTUALENVS_CREATE=1 \
    POETRY_CACHE_DIR=/tmp/poetry_cache

WORKDIR /app
COPY pyproject.toml poetry.lock ./
RUN poetry lock && poetry install --no-root && rm -rf $POETRY_CACHE_DIR

#### APP ####

FROM python:3.11.11-slim
WORKDIR /app
ENV VIRTUAL_ENV=/app/.venv \
    PATH="/app/.venv/bin:$PATH"

COPY --from=build ${VIRTUAL_ENV} ${VIRTUAL_ENV}
COPY src ./src
COPY resources ./resources

ENTRYPOINT [ "python", "src/main.py" ]
