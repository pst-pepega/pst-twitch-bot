#!/bin/bash

python_script="src/main.py"

while :
do
    # Run the Python script in the background, redirecting both stdout and stderr to a file
    python3.11 "$python_script" 2>&1 | tee output.log &

    # Capture the process ID of the Python script
    python_pid=$!

    # Set up a SIGTERM handler to gracefully terminate the Python script before killing it
    signal_handler() {
        kill -TERM $python_pid
        echo "Stopping the Python script..."
    }

    # Monitor the output for "Websocket closed"
    tail -f output.log | while read -r line
    do
        if [[ "$line" == *"Websocket connection was closed"* ]]; then
            echo "Detected 'Websocket closed' in the output. Restarting..."
            # Send a SIGTERM signal to the Python script
            trap signal_handler INT TERM

            # Wait for the SIGTERM handler to terminate the Python script
            sleep 10

            # Clear the SIGTERM handler
            trap - INT TERM

            break
        fi
    done
done
