import os
from twitchio.ext import commands
import yt_dlp
import requests

DISCORD_WEBHOOK_URL = "https://discord.com/api/webhooks/1197520898060726282/BfOKFVg_C96Qez59Jam5EAOrQieQPdYDwOXtE_VW62VDzUbyMVxym1j7aZRobmMbuoiv"

def get_video_duration(video_url):
    # Define options for yt-dlp
    ydl_opts = {
        'quiet': True,     # Do not print anything to stdout
        'extract_flat': True,  # Extract only metadata without downloading
    }

    # Create yt-dlp instance
    with yt_dlp.YoutubeDL(ydl_opts) as ydl:
        info = ydl.extract_info(video_url, download=False)
        duration = info.get('duration', 0)  # Duration in seconds

    return duration


def load_duration_from_file(file_path):
    try:
        with open(file_path, 'r') as file:
            duration = int(file.read().strip())
    except FileNotFoundError:
        duration = 0
    return duration


def save_duration_to_file(file_path, duration):
    with open(file_path, 'w') as file:
        file.write(str(duration))


def get_clip(clip_url: str):
    # Download clip using yt-dlp
    duration = get_video_duration(clip_url) + load_duration_from_file("resources/clip_duration.txt")
    save_duration_to_file("resources/clip_duration.txt", duration)
    with yt_dlp.YoutubeDL({}) as ydl:
        info = ydl.extract_info(clip_url, download=False)
        video_url = info['url']

    # Upload clip to Discord webhook
    file_name = f"{info['id']}.mp4"
    response = requests.get(video_url)
    with open(file_name, 'wb') as f:
        f.write(response.content)
    file = {'file': open(file_name, 'rb')}
    payload = {'content': 'New clip:', 'username': 'Twitch Clip Bot'}
    requests.post(DISCORD_WEBHOOK_URL, data=payload, files=file)

    # Delete the clip
    os.remove(file_name)

get_clip("https://clips.twitch.tv/HelpfulHonorableFinchBabyRage-mBjKIrNvgdn6ATbO")