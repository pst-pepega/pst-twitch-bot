import json

def load_car_data():
    with open("resources/json/steer_ratio.json") as f:
        car_data = json.load(f)
    return car_data

def find_cars_by_brand(brand):
    car_data = load_car_data()
    matching_cars = []

    for category, cars in car_data.items():
        for car in cars:
            if brand.lower() in car["Brand"].lower() or brand.lower() in car["Model"].lower():
                matching_cars.append(car)

    return matching_cars

# Example usage:
# brand = input("Enter the brand name (e.g., 'Aston Martin'): ")
# matching_cars = find_cars_by_brand(brand)

# if matching_cars:
#    print(f"Matching cars for '{brand}':")
#    for car in matching_cars:
#        print(f"- Model: {car['Model']}, Steering lock: {car['Steering lock']}")
#else:
#    print(f"No cars found for '{brand}'.")
