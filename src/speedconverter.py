def mph_to_kph(mph):
    mph = float(mph)
    return round(mph * 1.60934, 2)

def kph_to_mph(kph):
    kph = float(kph)
    return round(kph / 1.60934, 2)

# Example usage:
# mph = 60
# kph = mph_to_kph(mph)
# print(f"{mph} mph is equal to {kph} kph")
# Output: 60 mph is equal to 96.56 kph

# kph = 100
# mph = kph_to_mph(kph)
# print(f"{kph} kph is equal to {mph} mph")
# Output: 100 kph is equal to 62.14 mph
