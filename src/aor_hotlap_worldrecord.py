import pandas as pd

# Create a dictionary to map car names to their corresponding values
car_to_value = {
    'porsche': 1,
    'honda': 2,
    'bmw': 3,
    'audi': 4,
    'ferrari': 5,
    'mercedes': 6,
    'aston': 7,
    'mcLaren': 8,
    'mclarenevo': 9,
    'bentley': 10,
    'lambo': 11,
    'nissan': 12,
    'lexus': 13
    # Add more mappings as needed
}

# URL of the published Google Sheets document (Make sure it's publicly accessible)
sheet_url = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vSmt58P5tkTxOWr71MRA8FOjZkyBilGzQReL4yy7gR2wJWdd9fhlVrtR_KcvRxijdWneJKPH4iRdHBj/pubhtml'

# Read data from the Google Sheets document using pandas
dfs = pd.read_html(sheet_url, encoding='utf-8')

# Assuming the second tab is the one you want to work with, access it by index [1]
df = dfs[0]
#print(df)

# Now, drop the desired columns
# Replace the column labels below with the actual column labels you want to remove
df = df.drop(columns=['Unnamed: 0', 'Unnamed: 1', 'Unnamed: 2', 'Unnamed: 3', 'Unnamed: 27', 'Unnamed: 28', 'Unnamed: 29'])
#df = df.drop(columns=['Unnamed: 0', 'Unnamed: 1', 'Unnamed: 2', 'Unnamed: 3', 'Unnamed: 27'])

# Drop ranges below 16 except 21 (last update)
#df = df.drop(index=df.index[21:])
#df = df.drop(index=df.index[16:20])
df = df.drop(index=df.index[18:])
df = df.drop(index=df.index[1:16])

print(df)

# Convert all the values in the first row to lowercase
df.iloc[0] = df.iloc[0].str.lower()

def update_values():
	global df
	global dfs
		
	# URL of the published Google Sheets document (Make sure it's publicly accessible)
	sheet_url = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vSmt58P5tkTxOWr71MRA8FOjZkyBilGzQReL4yy7gR2wJWdd9fhlVrtR_KcvRxijdWneJKPH4iRdHBj/pubhtml'

	# Read data from the Google Sheets document using pandas
	dfs = pd.read_html(sheet_url, encoding='utf-8')

	# Assuming the second tab is the one you want to work with, access it by index [1]
	df = dfs[0]

	# Now, drop the desired columns
	# Replace the column labels below with the actual column labels you want to remove
	df = df.drop(columns=['Unnamed: 0', 'Unnamed: 1', 'Unnamed: 2', 'Unnamed: 3', 'Unnamed: 27', 'Unnamed: 28'])

	# Drop ranges below 16 except 21 (last update)
	#df = df.drop(index=df.index[21:])
	#df = df.drop(index=df.index[16:20])
	df = df.drop(index=df.index[18:])
	df = df.drop(index=df.index[1:16])

	#print(df)

	# Convert all the values in the first row to lowercase
	df.iloc[0] = df.iloc[0].str.lower()

def get_cars():
	# Get the car names (keys) from the dictionary
	car_names = list(car_to_value.keys())

	# Create a string with the list of car names
	possible_values_string = ", ".join(car_names)

	# Print the string
	print("The possible values are", possible_values_string, "👍")	
	return f"The possible values are: {possible_values_string} 👍"


def get_tracks():
	# Access row 0 and print values in comma-separated format
	row_values = df.iloc[0].tolist()
	output = ', '.join(str(value) for value in row_values)
	return output


def get_laptimes(track):
	column_label = df.columns[df.loc[0] == track].tolist()[0]
	lap_time = df.loc[16, column_label]
	record_holder = df.loc[17, column_label]
	print(f"The current record holder at {track} is {record_holder} with a lap time of {lap_time}.")
	return_list = []
	return_list.append(track)
	return_list.append(record_holder)
	return_list.append(lap_time)
	return return_list

def all_laptimes(track):
	result = dict()
	formatted_result = ""
	column_label = df.columns[df.loc[0] == track].tolist()[0]
	last_update = df.loc[19, column_label]
	last_update = f"last update: {last_update}"
	
	for car, index in car_to_value.items():
		# Calculate the row index in the DataFrame
		row_index = index + 2
		
		# Get the value_to_print from the DataFrame
		value_to_print_str = df.loc[row_index, column_label]
		value_to_print = pd.to_numeric(value_to_print_str, errors='coerce')
		print(f"{value_to_print}")
		
		# Check if the value_to_print is a valid numeric value (not NaN)
		if not pd.isna(value_to_print):
			# Add the car and numeric value_to_print to the result_dict
			result[car] = value_to_print

	# Sort the result_dict based on values from lowest to highest
	sorted_result_dict = dict(sorted(result.items(), key=lambda item: item[1]))

	# Iterate through the sorted_result_dict
	for car, value in sorted_result_dict.items():
    # Append the car and value to the formatted string
		formatted_result += f"{car.title()}: {value:.3f}\n"

	# Print the formatted result string
	print(f"{formatted_result}")
	return f"```\n{formatted_result}\n```\n {last_update}"