import os
from twitchio.ext import commands
import yt_dlp
import requests
from dotenv import load_dotenv
import subprocess


load_dotenv()
DISCORD_WEBHOOK_URL = os.getenv('ITB')

def get_video_duration(video_url):
    # Define options for yt-dlp
    ydl_opts = {
        'quiet': True,     # Do not print anything to stdout
        'extract_flat': True,  # Extract only metadata without downloading
    }

    # Create yt-dlp instance
    with yt_dlp.YoutubeDL(ydl_opts) as ydl:
        info = ydl.extract_info(video_url, download=False)
        if info:
            duration = info.get('duration', 0)  # Duration in seconds

    return duration


def load_duration_from_file(file_path):
    try:
        with open(file_path, 'r') as file:
            duration = int(file.read().strip())
    except FileNotFoundError:
        duration = 0
    return duration


def save_duration_to_file(file_path, duration):
    with open(file_path, 'w') as file:
        file.write(str(duration))


def get_clip(clip_url: str, user: str, channel: str):
    # Download clip using yt-dlp
    duration = get_video_duration(clip_url) + load_duration_from_file("resources/clip_duration.txt")
    save_duration_to_file("resources/clip_duration.txt", duration)
    with yt_dlp.YoutubeDL({}) as ydl:
        info = ydl.extract_info(clip_url, download=False)
        if info:
            video_url = info['url']

    # Upload clip to Discord webhook
    if info:
        file_name = f"{info['id']}.mp4"
        response = requests.get(video_url)
        with open(file_name, 'wb') as f:
            f.write(response.content)
        file = {'file': open(file_name, 'rb')}
        payload = {'content': f'New clip from {user} in <https://twitch.tv/{channel}>:', 'username': 'Twitch Clip Bot'}
        response = requests.post(DISCORD_WEBHOOK_URL, data=payload, files=file) # type: ignore

        # Check response status
        if response.status_code == 200:
            print("Webhook request sent successfully!")
        else:
            print(f"Failed to send webhook request. Status code: {response.status_code}")
            # compress_video(file_name, "temp.mp4", 23.0)
            # file = {'file': open("temp.mp4", 'rb')}
            # payload = {'content': f'New clip from {user} in <https://twitch.tv/{channel}>:', 'username': 'Twitch Clip Bot'}
            # response = requests.post(DISCORD_WEBHOOK_URL, data=payload, files=file) # type: ignore
            
            payload = {'content': f'New clip from {user} in <https://twitch.tv/{channel}>:\n{clip_url}', 'username': 'Twitch Clip Bot'}
            response = requests.post(DISCORD_WEBHOOK_URL, json=payload) # type: ignore

    if duration > 600:
        # Define payload to send in the webhook request
        payload = {
            "content": f"New clip duration is {duration} seconds. It meets the criteria for notification. <@83552350525980672>"
        }

        # Send webhook request
        response = requests.post(DISCORD_WEBHOOK_URL, json=payload) # type: ignore

        # Check response status
        if response.status_code == 200:
            print("Webhook request sent successfully!")
        else:
            print(f"Failed to send webhook request. Status code: {response.status_code}")


    # Delete the clip
    os.remove(file_name)

#get_clip("https://clips.twitch.tv/AmericanCalmPidgeonHassanChop-lWvwbgXdk4z6wwBG", "test", "test")