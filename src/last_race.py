from twitchio.ext import commands
import os
from dotenv import load_dotenv
import time
import schedule
import socket

import last_race_check

# Load environment variables from .env file
load_dotenv()

TOKEN = os.getenv('TOKEN')

BOT_NICKNAME = 'PST Bot'
BOT_TOKEN = TOKEN
CHANNEL_NAME = 'kylooooren'
ADDITIONAL_CHANNELS = ['sirfoch', 'ukog', 'haunted_1', 'emperalhero']

users_list = [
	{"id": 2683, "name": "Samir Foch", "twitch": "haunted_1"},
	{"id": 4215, "name": "George Boothby", "twitch": "haunted_1"},
	{"id": 1704, "name": "Bruhier Maedre", "twitch": "haunted_1"},
	# Add more user dictionaries here if needed
]


def send_chat_message(user):
	print(f"checking {user['name']} at {user['twitch']}")
	irc_server = "irc.chat.twitch.tv"
	port = 6667
	bot_username = BOT_NICKNAME
	oauth_token = f"oauth:{TOKEN}"

	print(user)
	message = last_race_check.send_it(user)
	print(message)
	# Connect to the IRC server
	irc = socket.socket()
	irc.connect((irc_server, port))

	# Authenticate with the access token (oauth token)
	irc.send(f"PASS {oauth_token}\n".encode("utf-8"))

	# Set the bot username
	irc.send(f"NICK {bot_username}\n".encode("utf-8"))

	# Send USER message (you can leave the real name and host empty)
	irc.send(f"USER {bot_username} 8 * :{bot_username}\n".encode("utf-8"))

	# Join the channel
	irc.send(f"JOIN #{user['twitch']}\n".encode("utf-8"))

	# Send the message
	irc.send(f"PRIVMSG #{user['twitch']} :{message}\n".encode("utf-8"))
	print(f"PRIVMSG #{user['twitch']} :{message}\n".encode("utf-8"))
	time.sleep(3)

	# Close the connection
	irc.close()

def send_message_help(user):
	message = last_race_check.send_it(user)
	return message	

def schedule_jobs_for_users(users_list):
	for user in users_list:
		# Schedule the function to run at :00 and :15 every hour for each user's Twitch channel
		schedule.every().hour.at(":25").do(send_chat_message, user)
		schedule.every().hour.at(":20").do(send_chat_message, user)


# Run the scheduler function for all users in the list
schedule_jobs_for_users(users_list)

# Run the scheduling loop
while True:
	schedule.run_pending()
	time.sleep(1)