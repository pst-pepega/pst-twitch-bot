import requests


def get_coordinates(query):
    # Nominatim API endpoint for geocoding
    url = f"https://nominatim.openstreetmap.org/search?q={query}&format=json&limit=1"
    headers = {
        'User-Agent': 'TwitchBot'  # Replace with your app name and email
    }
    
    try:
        # Make a GET request to the Nominatim API with headers
        response = requests.get(url, headers=headers)
        response.raise_for_status()  # Raise an error for bad status codes

        # Parse the JSON response
        location_data = response.json()
        if location_data:
            # Extract relevant information
            city = location_data[0].get('display_name', 'N/A')
            latitude = location_data[0]['lat']
            longitude = location_data[0]['lon']
            return city, latitude, longitude
        else:
            print(f"Location '{query}' not found.")
            return None, None, None

    except requests.exceptions.RequestException as e:
        print(f"An error occurred while fetching coordinates: {e}")
        return None, None, None


def get_weather(latitude, longitude):
    result = ""
    # Open-Meteo API endpoint
    url = f"https://api.open-meteo.com/v1/forecast?latitude={latitude}&longitude={longitude}&current_weather=true"

    try:
        # Make a GET request to the API
        response = requests.get(url)
        response.raise_for_status()  # Raise an error for bad status codes

        # Parse the JSON response
        weather_data = response.json()

        # Extract current weather information
        current_weather = weather_data.get('current_weather', {})
        temperature = current_weather.get('temperature')
        windspeed = current_weather.get('windspeed')
        weather_code = current_weather.get('weathercode')

        result += (f"Temperature: {temperature}°C — ")
        result += (f"Wind Speed: {windspeed} km/h")

        return result
    
    except requests.exceptions.RequestException as e:
        print(f"An error occurred while fetching weather data: {e}")

        return "Error. Rip Bozo."
    

def get_weather_query(query):
    city, latitude, longitude = get_coordinates(query)

    result = f"Current weather in {city}: "

    if latitude and longitude:
        print(f"Location: {city}")
        print(f"Latitude: {latitude}")
        print(f"Longitude: {longitude}")
        result += (get_weather(latitude, longitude))

    return result