import requests

def get_user_info(search_query, items = 1):
    if items > 1:
        results = []
        counter = 0
        if items > 10:
            items = 10
            results.append("Nice try motherfucker, but the maximum number of drivers that can be searched for is 10.")
    # Make the API request
    result = ""
    api_url = f"https://api2.lowfuelmotorsport.com/api/search/{search_query}"
    response = requests.get(api_url)
    
    # Check if the request was successful (status code 200)
    if response.status_code == 200:
        # Parse the JSON response
        data = response.json()

        # Check if the response contains data and drivers
        if 'data' in data and 'drivers' in data['data']:
            drivers = data['data']['drivers']

            # Check if there are any drivers in the response
            if drivers:
                # Extract information from the first driver
                if items == 1:
                    first_driver = drivers[0]
                    vorname = first_driver['vorname']
                    nachname = first_driver['nachname']
                    rating = first_driver['rating_by_sim'][0]['rating']
                    safety_rating = first_driver['safety_rating']
                    result = f"{vorname} {nachname} — SR: {safety_rating} & Elo: {rating}"
                    return result
                # Output the information
                else:
                    while counter < items:
                        driver = drivers[counter]
                        vorname = driver['vorname']
                        nachname = driver['nachname']
                        rating = driver['rating_by_sim'][0]['rating']
                        safety_rating = driver['safety_rating']
                        result = f"{counter+1}. {vorname} {nachname} — SR: {safety_rating} & Elo: {rating}"
                        results.append(result)
                        counter += 1
                    return results


            else:
                print("No drivers found in the response.")
        else:
            print("Invalid response format.")
    else:
        print(f"Error: {response.status_code}")


def get_user_id(search_query):
    # Make the API request
    api_url = f"https://api2.lowfuelmotorsport.com/api/search/{search_query}"
    response = requests.get(api_url)
    
    # Check if the request was successful (status code 200)
    if response.status_code == 200:
        # Parse the JSON response
        data = response.json()

        # Check if the response contains data and drivers
        if 'data' in data and 'drivers' in data['data']:
            drivers = data['data']['drivers']

            return (drivers[0]['rating_by_sim'][0]['user_id'])
        else:
            print("Invalid response format.")
    else:
        print(f"Error: {response.status_code}")
        