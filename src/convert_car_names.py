def convert_car_names(car: str):
    if car == "Bentley Continental":
        return "Bentley"
    
    if car == "Honda NSX GT3 Evo":
        return "Honda"
    
    if car == "Ford Mustang GT3":
        return "Ford"

    if car == "BMW M4 GT3":
        return "BMW"
    
    if car == "Porsche 992 GT3 R":
        return "Porsche"
    
    if car == "Ferrari 296 GT3":
        return "Ferrari"
    
    if car == "Nissan GT-R Nismo GT3":
        return "Nissan"
    
    if car == "McLaren 720S GT3 Evo":
        return "McLaren"

    if car == "Lamborghini Huracan GT3 EVO 2":
        return "Lamborghini"
    
    if car == "AMR V8 Vantage":
        return "AMR"
    
    if car == "Audi R8 LMS GT3 evo II":
        return "Audi"
    
    if car == "Mercedes-AMG GT3":
        return "Mercedes"