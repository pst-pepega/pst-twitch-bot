
import csv
from datetime import datetime, timedelta
import sys
import requests
import logging
import car_lookup

HOURS_FROM_CET = -1

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

def closest_future_date(date_str, date_list):
    target_date = datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S')
    logging.info(target_date)
    future_dates = [date for date in date_list if datetime.strptime(date, '%Y-%m-%d %H:%M:%S') > target_date]
    logging.info(future_dates[0])
    if future_dates:
        closest_date = min(future_dates, key=lambda x: datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
        return closest_date
    else:
        return None

def get_race_id():
    csv_file = 'resources/races.csv'  # Use the single file

    current_datetime = datetime.now() + timedelta(hours=(HOURS_FROM_CET*-1))  # Define the current time in the CET time zone
    target_datetime = current_datetime + timedelta(hours=1)  # Define the target as now + 1 hour, self time zone is UTC and the races.csv is UTC+1 so 1h ahead is 2 hours ahead.

    # Read and filter data
    with open(csv_file, 'r') as file:
        reader = csv.DictReader(file)
        rows = list(reader)

    # Filter rows for races within the target time frame
    future_rows = []
    for row in rows:
        row_datetime = datetime.strptime(row['Date and Time'], '%Y-%m-%d %H:%M:%S')
        if current_datetime <= row_datetime <= target_datetime:
            logging.info(row)
            future_rows.append(row)

    # Remove rows from the past
    updated_rows = [row for row in rows if datetime.strptime(row['Date and Time'], '%Y-%m-%d %H:%M:%S') >= current_datetime]

    # Overwrite the file with updated rows
    with open(csv_file, 'w', newline='') as file:
        writer = csv.DictWriter(file, fieldnames=reader.fieldnames)
        writer.writeheader()
        writer.writerows(updated_rows)

    if future_rows:
        return future_rows  # Return rows within the target time frame
    else:
        logging.info("No races within the target time frame.")
        return []


def find_drivers(race_id, driver_id):
    url = f"https://api2.lowfuelmotorsport.com/api/race/{race_id}"

    try:
        response = requests.get(url)
        json_data = response.json()

        if 'participants' in json_data:
            entrylist = json_data['participants']
            if 'entries' in entrylist:
                entries = entrylist['entries']
                for entry_info in entries:
#                    driver = entry_info['drivers'][0]
                    first_name = entry_info['vorname']
                    last_name = entry_info['nachname']
                    raceNumber = f"[{entry_info['raceNumber']}]"
                    car_id = entry_info['car_model']
                    id = entry_info['user_id']
                    car_name = f"({car_lookup.Car_lookup(car_id)})"
                    name = f"{first_name} {last_name}"
                    if id == driver_id:
                        return (f"{name.ljust(30)} {car_name.ljust(30)} {raceNumber}\n")
                    else:
                        logging.info(f"Driver {first_name} {last_name} is not in the provided list.")
            else:
                logging.info("No entries found for this race.")
        else:
            logging.info("No entrylist found in the JSON data.")

    except Exception as e:
        logging.info(f"An error occurred: {e}")


def format_discord_message(row, series):
    try:
        track = row.get('Track', 'Unknown Track')
        wet_chance = float(row.get('Wet Chance'))*100 if row.get('Wet Chance') != '' else "N/A"
        dry_chance = float(row.get('Dry Chance'))*100 if row.get('Dry Chance') != '' else "N/A"
        mixed_chance = float(row.get('Mixed Chance'))*100 if row.get('Mixed Chance') != '' else "N/A"
        avg_temp = float(row.get('Avg Temp')) if row.get('Avg Temp') != '' else "N/A"

        if wet_chance != "N/A":
            message = (
                f"""RACE ALERT: {series.upper()} — {track} — Wet: {wet_chance:.2f}%, Dry: {dry_chance:.2f}%, Mixed: {mixed_chance:.2f}% — Avg Temp: {avg_temp}°C"""
            )
        else:
            message = (
                f"**{series.upper()} Race Alert:\n**"
                f"Track: {track}\n"
            )
    except Exception as e:
        logging.error(f"Error formatting message: {e}")
        message = "Race information could not be processed."

    return message


def main(driver_id):
    messages = []
    entered_races = []

    rows = get_race_id()
    logging.info(f"{len(rows)} Rows found.")
    for row in rows:
        logging.info(f"Checking: {row['Event Name']}")
        message = []
        race_id = row['id']
        logging.info(race_id)
        print(row)
        series = row['Event Name']
        message.append(format_discord_message(row, series))
        driver_message = find_drivers(race_id, driver_id)
        if driver_message:
            entered_races.append(race_id)
        message.append(driver_message)
        logging.info("#####")
        logging.info("\n")
        logging.info(message)
        logging.info("\n")
        messages.append(message)
    
    logging.info("Execution of check race for streamer completed.")
    return messages, entered_races

