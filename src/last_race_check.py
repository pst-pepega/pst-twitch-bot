from __future__ import annotations
import requests
from datetime import datetime
from dateutil import tz
import pickle
import json
import dataclasses
from typing import List, Any

LFM_API = "https://api2.lowfuelmotorsport.com/api"

""" @dataclasses.dataclass
class LFMUsers():
    id: int
    name: str

@dataclasses.dataclass
class Config():
    users: list[LFMUsers]

    @classmethod
    def load_config(cls, config_file: str) -> Config:
        with open(config_file, "r") as user_fp:
            config = json.load(user_fp)
            users = []
            print(config)
            for user in config["users"]:
                users.append(LFMUsers(user["id"], user["name"]))

        return cls(users) """

def load_previous_races_ids(file_path: str) -> dict:
    try:
        with open(file_path, "rb") as file:
            return pickle.load(file)
    except FileNotFoundError:
        return {}

#config = Config.load_config("json/config.json")

def get_users_past_races(user_id: int) -> List[dict[str, Any]]:

    url = f"{LFM_API}/users/getUsersPastRaces/{user_id}?start=0&limit=5"
    response = requests.get(url)
    return response.json()

def check_if_race_in_db(db: dict, user_id: int, race_id: int) -> bool:

    if user_id not in db.keys():
        db[user_id] = race_id
        return False

    is_present = db[user_id] == race_id
    if not is_present:
        db[user_id] = race_id
    
    return is_present

def get_race_time(race_data: dict) -> datetime:

    race_date = datetime.strptime(race_data["race_date"], "%Y-%m-%d %H:%M:%S")

    race_date = race_date.replace(tzinfo=tz.tzstr("UTC+1"))
    race_date_local = race_date.astimezone(tz.tzlocal())

    return race_date_local

def check_last_race(user: dict):

    past_races = get_users_past_races(user['id'])

    if len(past_races) == 0:
        print(f"never raced")
        return

    last_race = past_races[0]
    race_id = last_race["race_id"]
    print(f"Last race ID: {race_id}")

    race_time = get_race_time(last_race)
    race_time_epoch = int(race_time.timestamp())
    position_delta = last_race['start_pos'] - last_race['finishing_pos']
    sr_change_diff = "📈" if last_race['sr_change'] > 0 else "📉" if last_race['sr_change'] < 0 else "∆"
    elo_change_diff = "📈" if last_race['rating_change'] > 0 else "📉" if last_race['rating_change'] < 0 else "∆"
    position_change_diff = "📈" if position_delta > 0 else "📉" if position_delta < 0 else "∆"
    best_of_week = ""
    if last_race["best_of_week"] == "true" : best_of_week = f"<:AYAYA:1102246176645988393>  Best result of the week for {user['name']} <:AYAYA:1102246176645988393>"


    message = f"Latest LFM race for {user['name']} \u2500 {position_change_diff} P{last_race['start_pos']} -> P{last_race['finishing_pos']} [{position_delta}] \u2500 {elo_change_diff} {last_race['rating_change']} Elo \u2500 {sr_change_diff} {last_race['sr_change']} SR [{last_race['incidents']} IP]"
    return message
    message_list.append(message)
    print(message)

    #send_discord_message(discord_message, config.discord_token)

    # if last_race['finishing_pos'] == 1:
    #     #send_discord_message("Congratulations on your victory! <:peepoheart:1112477859680899122> Well done!", config.discord_token)
    #     #send_discord_message("Congratulations on your victory <@{user.discord_id}! <:peepoheart:1112477859680899122> Well done!")
    #     print(f"Congratulations on your victory {user['name']}! <:peepoheart:1112477859680899122> Well done!")
    #     message_list.append(f"Congratulations on your victory {user['name']}! <:peepoheart:1112477859680899122> Well done!")
    # elif last_race['finishing_pos'] <= 3:
    #     #send_discord_message("You've clinched a spot on the podium! <:WICKED:1015967738985979925> Way to go, congrats!", config.discord_token)
    #     #send_discord_message("You've clinched a spot on the podium! <@{user.discord_id}! <:WICKED:1015967738985979925> Way to go, congrats!")
    #     print(f"You've clinched a spot on the podium! {user['name']}! <:WICKED:1015967738985979925> Way to go, congrats!")
    #     message_list.append(f"You've clinched a spot on the podium! {user['name']}! <:WICKED:1015967738985979925> Way to go, congrats!")

    # if last_race['start_pos'] == 1:
    #     #send_discord_message("Pole Position, Let's go Ayrton Senna! <:Binoculars:1101918533811707914>", config.discord_token)
    #     message_list.append("Pole Position, Let's go Ayrton Senna! <:Binoculars:1101918533811707914>")
    #     print("Pole Position, Let's go Ayrton Senna! <:Binoculars:1101918533811707914>")

    return message_list


def main():
    print("test")
    # race_ids = load_previous_races_ids("race_ids.pkl")

    # for user in config.users:
    #     print(f"User {user.name} (id: {user.id})")
    #     check_last_race(race_ids, user)
    #     print("-" * 20)

    # # Save race_ids to file
    # with open("race_ids.pkl", "wb") as file:
    #     pickle.dump(race_ids, file)

def send_it(user):    
    print(user)

    print(f"User {user['name']} (id: {user['id']})")
    message_list = "" 
    message_list = check_last_race(user)
    print("-" * 20)

    return message_list


if __name__ == "__main__":
    main()

