import pandas as pd
import re
from datetime import datetime, timedelta

def filter_data(df, track=None, min_avg_temp=None, max_avg_temp=None, min_mixed_chance=None, min_dry_chance=None, min_wet_chance=None, past_date=None):
    # Convert 'Date and Time' column to datetime
    df['Date and Time'] = pd.to_datetime(df['Date and Time'])

    # Apply filters based on provided criteria
    if track is not None:
        df = df[df['Track'] == track]
    
    if min_avg_temp is not None:
        df = df[df['Avg Temp'] >= min_avg_temp]

    if max_avg_temp is not None:
        df = df[df['Avg Temp'] <= max_avg_temp]

    if min_mixed_chance is not None:
        df = df[df['Mixed Chance'] >= min_mixed_chance]

    if min_dry_chance is not None:
        df = df[df['Dry Chance'] >= min_dry_chance]

    if min_wet_chance is not None:
        df = df[df['Wet Chance'] >= min_wet_chance]

    if past_date is not None:
        if past_date.lower() == 'now':
            past_date = pd.to_datetime('now') + pd.Timedelta(hours=1)
            # timezone is +2 so timedelta 2 hours
        else:
            past_date = pd.to_datetime(past_date)
        df = df[df['Date and Time'] >= past_date]

    return df


def parse_string(input_string: str):
    # Extracting values using regular expressions
    pattern = r'(track=([^=]+))?\s+(mintemp=(\d+))?\s?+(maxtemp=(\d+))?\s?+(mixed=(\d+))?\s?+(dry=(\d+))?\s?+(wet=(\d+))?'

    match = re.findall(pattern, input_string)
    match = match[0] if len(match) == 1 else match[1]

    if match:
        track_name = match[1] if match[1] != "" else None
        min_temp = match[3] if match[3] != "" else None
        max_temp = match[5] if match[5] != "" else None
        mixed = match[7] if match[7] != "" else None
        dry = match[9] if match[9] != "" else None
        wet = match[11] if match[11] != "" else None
    else:
        track_name = None
        min_temp = None
        max_temp = None
        mixed = None
        dry = None
        wet = None

    if min_temp:
        min_temp = int(min_temp)
    if max_temp:
        max_temp = int(max_temp)
    if mixed:
        mixed = float(int(mixed)/100)
    if dry:
        dry = float(int(dry)/100)
    if wet:
        wet = float(int(wet)/100)

    return [track_name, min_temp, max_temp, mixed, dry, wet]


def time_difference_to_target(target_time_str):
    # Convert the target time string to a datetime object
    target_time = datetime.strptime(target_time_str, "%Y-%m-%d %H:%M:%S")
    print(target_time)

    # Get the current time
    current_time = datetime.now() + pd.Timedelta(hours=1)

    # Calculate the time difference
    time_difference = target_time - current_time

    # Calculate days, hours, minutes
    days, seconds = divmod(time_difference.total_seconds(), 86400)
    hours, seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(seconds, 60)

    # Calculate months and years
    months, days_in_month = divmod(target_time.day - current_time.day, 30)
    print(months)
    years, months = divmod(months, 12)

    # Format the result
    if years > 0:
        return f"{int(years)} {'year' if int(years) == 1 else 'years'}"
    elif months > 0:
        return f"{int(months)} {'month' if int(months) == 1 else 'months'}"
    elif days > 0:
        return f"{int(days)} {'day' if int(days) == 1 else 'days'}"
    elif hours > 0:
        return f"{int(hours)} {'hour' if int(hours) == 1 else 'hours'}"
    elif minutes > 0:
        return f"{int(minutes)} {'minute' if int(minutes) == 1 else 'minutes'}"
    else:
        return "Less than a minute"


def get_data(series):
    #input_string = "!find_race mintemp=27 maxtemp=30 dry=88"
    #returned_list = parse_string(input_string)
    #print(returned_list)
    # Read data from CSV file
    df = pd.read_csv(f'resources/race_data/{series}.csv')

    # Example filtering
    filtered_df = filter_data(df, past_date='now')
    # Display the filtered DataFrame manually formatted as a table with tabs and headers
    for _, row in filtered_df.iterrows():
        return (f"Wet: {round(row['Wet Chance']*100,3)}% — Dry: {round(row['Dry Chance']*100, 3)}% — Mixed: {round(row['Mixed Chance']*100, 3)}% — Temps: {row['Avg Temp']} — {row['Date and Time']} [{time_difference_to_target(str(row['Date and Time']))}] — {row['Track']}\n")