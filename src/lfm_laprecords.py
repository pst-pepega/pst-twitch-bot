import requests

LFM_API = "https://api2.lowfuelmotorsport.com/api"

# Function to get car name by car ID
def get_car_name_by_id(car_id):
    response = requests.get(f"{LFM_API}/lists/getCars")
    
    # Check if the request was successful
    if response.status_code == 200:
        data = response.json()
        
        # Find the car with the given car_id
        for car in data:
            if car['car_id'] == car_id:
                return (car['car_name'])
        print(f"Car with ID {car_id} not found.")
    else:
        print(f"Failed to fetch data from API. Status code: {response.status_code}")


def get_race(track_id):
    # Fetch the JSON data from the API
    url = f"{LFM_API}/tracks/details/{track_id}"
    response = requests.get(url)
    data = response.json()

    result = ""

    # Extract and print the records for GT3 races (0 to 9)
    try:
        gt3_records = data['records']['GT3']['race']
        for i in range(10):
            lap = gt3_records[i]['lap']
            driver = gt3_records[i]['driver']
            #car = get_car_name_by_id(gt3_records[i]['car'])
            #print(f"{lap} - {driver} - {car}")
            result += (f"{lap} - {driver} || ")
    except KeyError as e:
        print(f"Key error: {e}")
    except IndexError as e:
        print(f"Index error: {e}")

    return result[:-4]


def get_qualifying(track_id):
    # Fetch the JSON data from the API
    url = f"{LFM_API}/tracks/details/{track_id}"
    print(url)
    response = requests.get(url)
    data = response.json()

    result = ""

    # Extract and print the records for GT3 races (0 to 9)
    try:
        gt3_records = data['records']['GT3']['qualifying']
        for i in range(10):
            lap = gt3_records[i]['lap']
            driver = gt3_records[i]['driver']
            #car = get_car_name_by_id(gt3_records[i]['car'])
            #print(f"{lap} - {driver} - {car}")
            result += (f"{lap} - {driver} || ")
    except KeyError as e:
        print(f"Key error: {e}")
    except IndexError as e:
        print(f"Index error: {e}")

    return result[:-4]
