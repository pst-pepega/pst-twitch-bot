import requests

PRO_SERIES_ID = 503

def get_list():
    result = ""
    url = f"https://api2.lowfuelmotorsport.com/api/v2/seasons/getSeriesPenalties/{PRO_SERIES_ID}"

    try:
        # Fetch JSON data from the API
        response = requests.get(url)
        data = response.json()

        # Print the "nachname" and "penalty points" fields for the first 10 items
        for entry in data[:10]:
            nachname = entry.get("nachname", "N/A")
            penalty_points = entry.get("penalty_points", "N/A")

            result += (f'{nachname}: {penalty_points} — ')

    except requests.exceptions.RequestException as e:
        print(f"Error fetching data: {e}")
    return(result[:-2])

def get_name(driver):
    url = f"https://api2.lowfuelmotorsport.com/api/v2/seasons/getSeriesPenalties/{PRO_SERIES_ID}"

    try:
        # Fetch JSON data from the API
        response = requests.get(url)
        data = response.json()

        # Print the "nachname" and "penalty points" fields for the first 10 items
        for entry in data:
            if entry.get("nachname", "N/A") == driver:   
                penalty_points = entry.get("penalty_points", "N/A")
                return (f'{entry.get("vorname", "N/A")} {entry.get("nachname", "N/A")}: {penalty_points}')

        return "Clean driver detected, try again with a dirty driver."
    except requests.exceptions.RequestException as e:
        print(f"Error fetching data: {e}")       
     