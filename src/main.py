from twitchio.ext import commands, routines
import os
from dotenv import load_dotenv
import asyncio
import re
import requests
import emoji
import json
import logging
import math
import pandas as pd
from datetime import datetime, timedelta

import fuel_calc
import most_active
import steer_ratio
import last_race_check
import pitlane
import lfm_race_data
import lfmproweather
import stocks
import lfm_scheduler
import lfmhl
import BOP
import entrylist
import laprecords
import userstats
import converters
import elocalc
import lfm_penaltypoints
import webhook
import lfm_search
import weathersim
import find_race
import discord_webhook_upload_acc_clip
import speedconverter
import user_stats_comparison
import lfm_laprecords
import weather
import convert_car_names
import pst_lfm
import check_race_for_streamer

# Configure the logger
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')


# Load environment variables from .env file
load_dotenv()

TOKEN = os.getenv('TOKEN')
ITB = os.getenv('ITB')
SDL = os.getenv('SDL')

BOT_NICKNAME = 'PST Bot'
BOT_TOKEN = TOKEN
CHANNEL_NAME = 'kylooooren'
ADDITIONAL_CHANNELS = []
MODERATORS = "Moderator"  # Replace with the actual moderator role name
DATA_DIR = "counter_data"  # Directory to store counter data files
invalid_counters = {}
CLIENT_ID = "23h2sq4je3tctmxh7ppv0ee7c4ra45"


with open("resources/json/users_list.json", "r") as file:
    users_list = json.load(file)
    
for user in users_list:
    ADDITIONAL_CHANNELS.append(user["twitch"])

#with open("resources/sponsored_messages.txt", "r") as file:
#    sponsored_messages = [line.strip() for line in file]

#print(f"Read the sponsored messages: {sponsored_messages}")

class TwitchBot(commands.Bot):

    def __init__(self):
        #initial_channels = [f"#{channel}" for channel in [CHANNEL_NAME] + ADDITIONAL_CHANNELS]
        initial_channels = [f"#{channel}" for channel in ADDITIONAL_CHANNELS]
        logging.info(initial_channels)
        super().__init__(token=BOT_TOKEN, nick=BOT_NICKNAME, client_id=CLIENT_ID, prefix='!', initial_channels=initial_channels)
    
        # initialise some variables so I don't compare to non-existant variable
        self.pepog_time = datetime.now()
        self.dinkdonk_time = datetime.now()
        self.sdl_time = datetime.now()
        self.itb_time = datetime.now()
        self.valencia_time = datetime.now()
        self.drivers_elo = dict()
        self.drivers_sr = dict()
        self.drivers_races = {
                                1704: [],
                                2683: [],
                                4215: []
                            }


    async def event_ready(self):
        logging.info(f'Logged in as {self.nick}')        
        users = pst_lfm.load_user_list()
        for user in users:
            last_race = await pst_lfm.set_last_race(user['id'], user['name'], user['twitch'])
            if last_race:
                logging.info(f"Set last race for {user['twitch']} as {last_race}")
            else:
                logging.info(f"Did not need to set new race id for {user['twitch']}")
        logging.info("We are ready.")
        await asyncio.sleep(10)
        # Start the routine when the bot is ready
        self.acc_status_notification.start()
        self.lfm_race_reminder.start()
        self.lfm_notifications.start()


    # Define the routine to run every 10 minutes
    @routines.routine(seconds=600)
    async def lfm_notifications(self):
        users = pst_lfm.load_user_list()
        #users = [{"id": 1704, "name": "Emir al-Batee'", "twitch": "kylooooren"}]
        for user in users:
            id = user['id']
            name = user['name']
            twitch = user['twitch']
            messages, sr, elo, last_race_id = await pst_lfm.check_last_race(id, name, twitch)

            if messages:
                for message in messages:
                    await self.send_message(twitch, message)

                logging.info(f"Send {len(messages)} to {twitch}")
            else:
                logging.info(f"Race result already sent to {twitch}")

            
            if twitch in self.drivers_sr:
                if not sr:
                    sr = 0.00
                self.drivers_sr[twitch] += sr
            else:
                self.drivers_sr[twitch] = sr

            
            if twitch in self.drivers_elo:
                if not elo:
                    elo = 0
                self.drivers_elo[twitch] += elo
            else:
                    self.drivers_elo[twitch] = elo

            if last_race_id:
                try:
                    if last_race_id in self.drivers_races[id]:
                        self.drivers_races[id].remove[last_race_id]
                        logging.info(f"###")
                        logging.info(f"Removed {last_race_id} from {id}")
                        logging.info(f"###")
                except:
                    logging.info("Driver not in the VIP list.")
            

            # rounding
            self.drivers_elo[twitch] = round(self.drivers_elo[twitch], 3)
            self.drivers_sr[twitch] = round(self.drivers_sr[twitch], 3)

            if messages:
                await self.send_message(twitch, f"Daily LFM Changes: {self.drivers_elo[twitch]} Elo and {self.drivers_sr[twitch]} SR.")


    # Define the routine to run every 10 minutes
    @routines.routine(seconds=1730)
    async def lfm_race_reminder(self):
        users = []
        #users = pst_lfm.load_user_list()
        users.append({"id": 1704, "name": "Emir al-Batee'", "twitch": "kylooooren"})
        users.append({"id": 2683, "name": "Samir Foch", "twitch": "sirfoch"})
        users.append({"id": 4215, "name": "George Boothby", "twitch": "UKOG"})
        for user in users:
            id = user['id']
            twitch = user['twitch']
            messages, entered_races = check_race_for_streamer.main(id)
            logging.info(f"Messages: {messages}")
            for message in messages:
                if message[1]:
                    await asyncio.sleep(10)
                    for sub_message in message:
                        await self.send_message(twitch, sub_message)
            
            if entered_races:
                self.drivers_races[id].extend(entered_races)
                
                logging.info(f"### Entered races for {id} ###")
                logging.info(self.drivers_races[id])

    
    @routines.routine(seconds=600)
    async def acc_status_notification(self):
        logging.info("####################")
        temp_message = None
        logging.info("RUNNING ACC STATUS NOTIFICATION")
        file_path = 'acc_status.txt'
        with open(file_path, 'r') as file:
            file_content = file.read()
        if file_content == "UP":
            servers_up = True
        else:
            servers_up = False
        url = "https://api2.lowfuelmotorsport.com/api/accstatus"
        request = requests.get(url)
        if request.status_code != 200:
            logging.error(
                f"Failed to execute request {url}, {request.status_code}: {request.text}"
            )
            status = None
        else:
            status = request.json()

        if status is None:
            logging.info("Some fuckery going on, can't get acc status")
        else:
            logging.info(f"ACC STATUS: {status.get('text')} - Last check was: {servers_up}")
            text = status.get("text")
            if "offline" in text.lower():
                if servers_up:
                    temp_message = "ACC Servers just went OFFLINE :)"
                    with open(file_path, 'w') as file:
                        file.write("DOWN")
                        file.flush()
                else:
                    temp_message = None
            else:
                if servers_up:
                    temp_message = None
                else:
                    temp_message = "ACC Servers just went ONLINE :) Can I get a POGGERS in chat for Rwaggy? KEKW"
                    with open(file_path, 'w') as file:
                        file.write("UP")
                        file.flush()

            logging.info(temp_message)
            if temp_message:
                users = pst_lfm.load_user_list()
                for user in users:
                    id = user['id']
                    name = user['name']
                    twitch = user['twitch']
                   
                    logging.info(f"#ACC STATUS: Message sent to {twitch}: {temp_message}")
                    await self.send_message(twitch, temp_message)

    
    async def send_message(self, channel: str, message: str):
        # Send a message to a specific channel
        ch = self.get_channel(channel)
        await ch.send(message)


    async def event_message(self, message):
        if message.author and message.author.name.lower() != self.nick.lower():
            if not message.content.startswith('!'):  # Check if the message is not a command
                logging.info(f"Regular Message received in {message.channel}: {message.content}")
                await self.handle_regular_message(message)
            else:
                logging.info(f"Command Message received {message.channel}: {message.content}")
                await self.handle_commands(message)


    async def handle_regular_message(self, message):        # This function will be called for every message, both commands and regular messages
        current_time = datetime.now()

        if "pepoG" in message.content and current_time >= self.pepog_time:
            await message.channel.send("pepoG")
            self.pepog_time = datetime.now() + timedelta(minutes=1)
        elif "pepoG" in message.content:
            logging.info(f"{current_time} < {self.pepog_time}")

        if "PepoG" in message.content and current_time >= self.pepog_time:
            await message.channel.send("PepoG")
            self.pepog_time = datetime.now() + timedelta(minutes=1)
        elif "PepoG" in message.content:
            logging.info(f"{current_time} < {self.pepog_time}")

        if "DinkDonk" in message.content and current_time >= self.dinkdonk_time:
            if message.content == "DinkDonk":
                await message.channel.send("DinkDonk")
            elif message.content == "DinkDonk DinkDonk":
                await message.channel.send("DinkDonk DinkDonk")
            elif message.content == "DinkDonk DinkDonk DinkDonk":
                await message.channel.send("DinkDonk DinkDonk DinkDonk")
            self.dinkdonk_time = datetime.now() + timedelta(minutes=1)
            
        elif "DinkDonk" in message.content:
            logging.info(f"{current_time} < {self.dinkdonk_time}")
        
        if "valencia " in message.content.lower() and current_time >= self.valencia_time:
            await message.channel.send("@Haunted_1, they said Valencia, not Ricardo Tormo. 😲")
            self.valencia_time = datetime.now() + timedelta(minutes=1)
        elif "valencia" in message.content:
            logging.info(f"{current_time} < {self.valencia_time}")


    # Define the !fuel command
    @commands.command(name='fuel') # type: ignore
    async def cmd_fuel(self, ctx):
        command_parts = ctx.message.content.split()
        logging.info(f"!fuel {command_parts}")
        if len(command_parts) != 4:
            await ctx.send(f"!fuel [duration (mins)] [fuel per lap] [laptime (1:42.5 or 1:42.50 or 1:42.500)]")
        else:
            fuel_parts = fuel_calc.split_fuel_string(ctx.message.content)
            meme = fuel_calc.fuel_calc(fuel_parts[0], fuel_parts[1], fuel_parts[2])
            for memes in meme:
                await ctx.send(memes)
                await asyncio.sleep(1)
                #print(memes)


    # Define the !mostactive command
    @commands.command(name='mostactive') # type: ignore
    async def cmd_mostactive(self, ctx):
        twitch_name = ctx.channel.name
        if ctx.message.content == "!mostactive":
            logging.info(f"!mostactive {input}")
            for user in users_list:
                if user["twitch"] == twitch_name:
                    name = user['name']

        logging.info(len(ctx.message.content.split()))

        if "id" in ctx.message.content:
            if ctx.message.content.split()[1] == "id":
                await ctx.send(most_active.check_with_id(ctx.message.content.split()[2]))

        elif "pos" in ctx.message.content:
            await ctx.send(most_active.most_active_command(int(ctx.message.content.split()[2])))

        else:
            try:
                name = ctx.message.content.split('!mostactive ')[1]
            except:
                name = name
            logging.info(name)
            position = most_active.find_user_position(name)
            if position == None:
                await ctx.send("Can't find them in the top 100 most 'active' LFM Drivers. :(")
            else:
                await ctx.send(f"{name} is in position {position}")
                await asyncio.sleep(1)
                await ctx.send(most_active.most_active_command(int(position)))


    # Define the !steer_ratio command
    @commands.command(name='steerratio') # type: ignore
    async def cmd_steer_ratio(self, ctx):
        print(ctx.message.content.split("!steerratio ", 1)[1])
        message_to_print = steer_ratio.find_cars_by_brand(ctx.message.content.split("!steerratio ", 1)[1])
        for _ in message_to_print:
            await ctx.send(_)
            await asyncio.sleep(1.5)


    # Define the !last_race command
    @commands.command(name='lastrace') # type: ignore
    async def cmd_last_race(self, ctx):
        twitch_name = ctx.channel.name
        logging.info(f"!lastrace {twitch_name}")
        try:
            input = ctx.message.content.split("!lastrace ", 1)[1]
            print(len(input))
            user_id = lfm_search.get_user_id(input)
            user_name = input
            user = {'name': user_name, 'id': user_id}
            logging.info(user)
            _ = last_race_check.send_it(user)
            await ctx.send(_)
        except:
            input = twitch_name
            logging.info(f"!lastrace {input}")
            if len(input) > 0:
                for user in users_list:
                    if user["twitch"] == input:
                        logging.info(f"!lastrace {input}")
                        _ = last_race_check.send_it(user)
                        await ctx.send(_)
        try:
            input = int(input)
            logging.info("input is number.")
            input = {"id": input, "name": ctx.author.name}
            print(input)
            _ = last_race_check.send_it(input)
            await ctx.send(_)
        except:
            if len(input) > 0:            
                for user in users_list:
                    if user["name"] == input:
                        logging.info(user)
                        _ = last_race_check.send_it(user)
                        await ctx.send(_)
            else:
                await ctx.send("Either input a lfm id or don't for streamer.")
    

    # Define the !pit command
    @commands.command(name='pit') # type: ignore
    async def cmd_pit(self, ctx):
        try:
            cmd = ctx.message.content.split("!pit ", 1)[1]
            cmd = cmd.split()
            logging.info(f"pit  {cmd}")

            logging.info(cmd)

            logging.info(f"Track: {cmd[0]}")

            track = converters.to_lfm_track_name(cmd[0]).title()
            logging.info(track)

            result = pitlane.find_closest_track(track)
            logging.info(result)
            
            if(cmd[1].lower() == "dt"):
                await ctx.send(f"{result['DT Time']}s at {result['name']}")
                await asyncio.sleep(1.5)
                await ctx.send("Note: Pitlane time. Add slowing down, accelerating and remove time it takes to go from pit entry to exit ON track.")
            
            elif(cmd[1].lower() == "sg30"):
                await ctx.send(f"{result['Tyre Change']}s at {result['name']}")
                await asyncio.sleep(1.5)
                await ctx.send("Note: Pitlane time. Add slowing down, accelerating and remove time it takes to go from pit entry to exit ON track.")
            
            elif(cmd[1].lower() == "pit"):
                await ctx.send(f"{result['Tyre Change']}s at {result['name']}")
                await asyncio.sleep(1.5)
                await ctx.send("Note: Pitlane time. Add slowing down, accelerating and remove time it takes to go from pit entry to exit ON track.")
            
            elif(cmd[1].lower() == "1"):
                await ctx.send(f"{result['1 Litre Refuel']}s at {result['name']}")
                await asyncio.sleep(1.5)
                await ctx.send("Note: Pitlane time. Add slowing down, accelerating and remove time it takes to go from pit entry to exit ON track.")
            
            else:
                await ctx.send("try !pit [track] [dt | sg30 | pit | 1]")
        except Exception as e:
            await ctx.send("try !pit [track] [dt | sg30 | pit | 1]")
            logging.info(e)


    # Define the !wr command
    @commands.command(name='wr') # type: ignore
    async def wr(self, ctx):
        track_name = ctx.message.content.replace("!wr ", "")

        logging.info(f"Track: {track_name}")

        track_name = converters.to_lfm_track_name(track_name)

        data = lfmhl.world_record(track_name)
        driver = f"{data['vorname']} {data['nachname']}"
        car = data['car_name']
        laptime = data['lap']
        date = data['lapdate']
        track = data['track_name']

        await ctx.send(f"{driver} holds the world record at {track} in the {car} with a laptime of {laptime} ({date})")
                    

    # Define the !race command
    @commands.command(name='race') # type: ignore
    async def race(self, ctx):
        try:
            cmd = ctx.message.content.split("!race ", 1)[1]
            cmd = cmd.replace("#", "")
            race_info, result = lfm_race_data.get_race(cmd)
            await ctx.send(f"{race_info} — {result}")
        except: await ctx.send(f"Didn't work. :(")
            

    # Define the !stocks command
    @commands.command(name='stocks') # type: ignore
    async def stocks(self, ctx):
        try:
            try:
                cmd = ctx.message.content.split("!stocks ", 1)[1]
            except:
                cmd = "UKOG.L"
            print(cmd)
            data = stocks.get_data(cmd)
            await ctx.send(data)
        except:
            await ctx.send("No.")


    # Define the !lfm command
    @commands.command(name='lfm') # type: ignore
    async def lfm(self, ctx):
        
        args = ctx.message.content.replace("!lfm ", "")
        print(args)
        if(len(args) == 0):
            await ctx.send(f"You forgot the track bozo.")
            return
    
        logging.info(args)
        logging.info(f"Track: {args}")
        
        args = converters.to_lfm_track_name(args)

        message_to_print = lfm_scheduler.find_track_by_input(args)
        for _ in message_to_print:
            await ctx.send(_)
            await asyncio.sleep(1.5)


    # Define the !next_week command
    @commands.command(name='nextweek') # type: ignore
    async def next_week(self, ctx):
        series_input = ctx.message.content

        logging.info(series_input) 

        if "gt3" in series_input or "gt4" in series_input or "single" in series_input:
            if len(series_input) < 2:
                await ctx.send("Invalid input. Try gt3, gt4 or single.")
            series_mapping = {
                "gt3": {"GT3 Series", "GT3+GT4 Multiclass Series", "Duo Cup", "Sprint Series", "Pro Series"},
                "gt4" : {"GT4 Series", "GT3+GT4 Multiclass Series"},
                "single": {"Single Make Series"}
               # "bmw": {"BMW M2 Series"}
            }
            events = lfm_scheduler.find_next_week()
            series_input = series_input.replace(f"!{self.next_week.name} ", "")
            filtered_events = [event for event in events if event['series'] in series_mapping.get(series_input.lower(), set())]
            print(filtered_events)
            for event in filtered_events:
                await ctx.send(f"{event['series']}: {event['track']}")
                await asyncio.sleep(1.5)
        else:
            events = lfm_scheduler.find_next_week()
            for event in events:
                await ctx.send(f"{event['series']}: {event['track']}")
                await asyncio.sleep(1.5)
        
        
    # Define the !current_week command
    @commands.command(name='currentweek') # type: ignore
    async def current_week(self, ctx):
        series_input = ctx.message.content

        logging.info(series_input) 

        if "gt3" in series_input or "gt4" in series_input or "single" in series_input:
            if len(series_input) < 2:
                await ctx.send("Invalid input. Try gt3, gt4 or single.")
            series_mapping = {
                "gt3": {"GT3 Series", "GT3+GT4 Multiclass Series", "Duo Cup", "Sprint Series", "Pro Series"},
                "gt4" : {"GT4 Series", "GT3+GT4 Multiclass Series"},
                "single": {"Single Make Series"}
               # "bmw": {"BMW M2 Series"}
            }
            events = lfm_scheduler.find_current_week()
            series_input = series_input.replace(f"!{self.next_week.name} ", "")
            filtered_events = [event for event in events if event['series'] in series_mapping.get(series_input.lower(), set())]
            print(filtered_events)
            for event in filtered_events:
                await ctx.send(f"{event['series']}: {event['track']}")
                await asyncio.sleep(1.5)
        else:
            events = lfm_scheduler.find_current_week()
            for event in events:
                await ctx.send(f"{event['series']}: {event['track']}")
                await asyncio.sleep(1.5)


    @commands.command(name='thisweek') # type: ignore
    async def this_week(self, ctx):
        await self.current_week(ctx)
    

    # Define the !teemu command
    @commands.command(name='teemu') # type: ignore
    async def teemu(self, ctx):
        url = "https://popometer.io/setups?user=Teemu+Karppinen?ref=u428"
        emoji_text = f"Get your latest Fehlkauf.json on 💩meter \U0001F449 {url}"
        await ctx.send(f"{emoji_text} or you can ask him for a free sample on Discord: https://discord.com/users/237955080324644865")


    # Define the !lfmhl command
    @commands.command(name='lfmhl') # type: ignore
    async def lfmhl(self, ctx):
        arguments = ctx.message.content[len('!lfmhl '):].lstrip("!lfmhl ")
        args = arguments.split(" - ")
        logging.info(args)

        logging.info(f"Track: {args[0]}")

        args[0] = converters.to_lfm_track_name(args[0])

        logging.info(args)
        sleep_time = 500
        logging.info(arguments)

        if("help" in arguments):
            await ctx.send("!lfmhl [Track] - [Name] - [Car] - [Patch (optional)]")
            await ctx.send("For Example: Imola Porsche ↪ !lfmhl Imola - None - Porsche")
        elif len(args) == 4:
            logging.info(f"lfmhl.get_data({args[0]}, {args[1]}, {args[2]}, {args[3]}")

            matching_hotlaps = lfmhl.get_data(args[0], args[1], args[2], args[3])
            if len(matching_hotlaps) == 0:
                await ctx.send("no results.")
            for hotlaps in matching_hotlaps:
                await ctx.send(f"{hotlaps['vorname']} {hotlaps['nachname']} - {hotlaps['car_name']} - {hotlaps['lap']} - {hotlaps['track_name']} - {hotlaps['version']} - {hotlaps['lapdate']} UTC")
                await asyncio.sleep(sleep_time/1000)
                sleep_time *= 1.1
        elif len(args) == 3:
            logging.info(f"lfmhl.get_data({args[0]}, {args[1]}, {args[2]}")

            logging.info(args[1])

            if args[1] == "all":
                args[1] = "none"

            logging.info(args[1])
                
            matching_hotlaps = lfmhl.get_data(args[0], args[1], args[2], "")
            if len(matching_hotlaps) == 0:
                await ctx.send("no results.")
            for hotlaps in matching_hotlaps:
                await ctx.send(f"{hotlaps['vorname']} {hotlaps['nachname']} - {hotlaps['car_name']} - {hotlaps['lap']} - {hotlaps['track_name']} - {hotlaps['version']} - {hotlaps['lapdate']} UTC")
                await asyncio.sleep(sleep_time/1000)
                sleep_time *= 1.1

        elif len(args) == 2:
            logging.info(f"lfmhl.get_data({args[0]}, {args[1]}")

            matching_hotlaps = lfmhl.get_data(args[0], args[1], "", "")
            if len(matching_hotlaps) == 0:
                await ctx.send("no results.")
            for hotlaps in matching_hotlaps:
                await ctx.send(f"{hotlaps['vorname']} {hotlaps['nachname']} - {hotlaps['car_name']} - {hotlaps['lap']} - {hotlaps['track_name']} - {hotlaps['version']} - {hotlaps['lapdate']} UTC")
                await asyncio.sleep(sleep_time/1000)
                sleep_time *= 1.1
        elif len(args) == 1:
            loop_count = 0
            logging.info(f"lfmhl.car_records({args[0]}")


            matching_hotlaps = lfmhl.car_records(args[0])
            if len(matching_hotlaps) == 0:
                await ctx.send("no results.")

            completed_loops = 1
            loops = len(matching_hotlaps)
            result = f"{completed_loops}/{math.ceil(loops/5)} → "

            for car, lap_time in matching_hotlaps.items():
                if loop_count % 4 == 0 and loop_count != 0:
                    result += f"{car}: {lap_time}"
                else:
                    result += f"{car}: {lap_time} — "
                loop_count += 1

                if loop_count >= 5:
                    completed_loops += 1
                    await ctx.send(result)
                    logging.info(result)
                    result = f"{completed_loops}/{math.ceil(loops/5)} → "
                    loop_count = 0
                    await asyncio.sleep(1)
            
            if loop_count > 0:
                logging.info(result)    
                await ctx.send(result)
        else:
            await ctx.send("!lfmhl [Track] - [Name] - [Car] - [Patch (optional)]")
            await ctx.send("For Example: Imola Porsche ↪ !lfmhl Imola - None - Porsche")


    @commands.command(name='adduser') # type: ignore
    async def adduser(self, ctx):
        logging.info(f"!adduser has been used.")
        if ctx.message.content.lstrip("!adduser ") == "help":
            await ctx.send("!adduser [your lfm id] - [your lfm name or whatever you want to be called]")
        input = ctx.message.content.lstrip("!adduser ").split(" - ")

        id = int(input[0])
        name = input[1]
        twitch = ctx.author.name

        new_user = {"id": id, "name": name, "twitch": twitch}
        users_list.append(new_user)

        with open("resources/json/users_list.json", "w") as file:
            json.dump(users_list, file, indent=4, ensure_ascii=False)

        
        await self.join_channels([twitch])
        logging.info(f'Joined channel: {twitch}')

        await ctx.send(f"User {name} added!")
        await ctx.send(f"@{ctx.author.name}, make sure to make me mod/vip in your channel!")


    # Define the !limit command
    @commands.command(name='limit') # type: ignore
    async def limit(self, ctx):
        # Split the message content on spaces
        ctx.message.content = ctx.message.content.replace(". ", "#")
        logging.info(ctx.message.content)
        command_parts = ctx.message.content.split(" ")
        if "help" in ctx.message.content:
            ctx.send("!limit [track] [number]%")
        if len(command_parts) >= 1:
            track = command_parts[1]
            track = converters.to_lfm_track_name(track)
            logging.info(f"!limit {track}")
            while track:
                try:
                    logging.info(f"!limit {command_parts}")
                    print(f"Trying: {track}")
                    wr_list = lfmhl.world_record(track.lower())
                    logging.info(wr_list)
                    logging.info(wr_list['lap'])
                    time_str = wr_list['lap']
                    logging.info("time_str: " + time_str)
                    factor = float(command_parts[2].replace("%",""))*0.01+1

                    logging.info(f"{time_str} {factor}")

                    minutes, seconds = map(float, time_str.split(':'))
                    logging.info(f"1. {minutes} {seconds}")
                    seconds = minutes * 60 + seconds
                    logging.info(f"2. {seconds}")
                    new_seconds = seconds * factor
                    logging.info(f"3. {new_seconds}")
                    minutes, new_seconds = divmod(new_seconds, 60)
                    logging.info(f"4. {minutes} {new_seconds}")
                    milliseconds = (new_seconds - int(new_seconds)) * 1000
                    logging.info(f"5. {milliseconds}")
                    minutes = int(minutes)


                    logging.debug(f"{minutes:02}:{int(new_seconds):02}.{int(milliseconds):03}")
                    await ctx.send(f"{minutes:02}:{int(new_seconds):02}.{int(milliseconds):03}")
                    break  # Break out of the loop if sending is successful
                except:
                    track = track[:-1]  # Remove the last character and try again


    @commands.command(name='commands') # type: ignore
    async def list_commands(self, ctx):
        input = ctx.message.content.replace(f"!{self.list_commands.name} ", "")
        if input == "all":
            command_names = ', '.join([command for command in self.commands])
            response = f"Available commands: {command_names}"
            await ctx.send(response)
        else:
            response = '''
Most useful commands are: fuel, lastrace, race, nextweek,
currentweek, bop, realbop, proseries, accdown/accstatus,
livetiming, lfmlaprecords
'''
        await ctx.send(response)


    @commands.command(name='discordbot') # type: ignore
    async def discordbot(self, ctx):
        await ctx.send("If you want to see some more, on discord, you can try the discord bot :) https://pst-bot.skillissue.be/")


    @commands.command(name='bop') # type: ignore
    async def bop(self, ctx):
        
        await ctx.send("We are checking, we are checking.")

        track = ctx.message.content.replace("!bop ", "")

        logging.info(f"Track: {track}")

        track = converters.to_lfm_track_name(track)

        closest_track = BOP.find_closest_track(BOP.get_all_tracks(), track)

        result = BOP.get_bop(track)
        print(result)
        #result = result[(result['relevant'] == 'X') | (result['car_name'] == 'Ferrari 488 GT3 Evo') | (result['bop'] == 0)] 
        logging.info(result)

        # Initialize an empty message string
        message = f"{closest_track}: "

        # Remove the trailing " — " from the message
        #message = message[:-3]

        target = BOP.get_targettime(track)

        logging.info(track)
        result.drop("car_year", axis=1, inplace=True)
        result = result.rename(columns={'relevant': 'rel'})
        result = result.rename(columns={'bop_raw': 'raw'})
        result['target_dif'] = result['target_dif'].apply(lambda x: x[0] + x[4:] if x[1:3] == '00' else x)
        result = result.rename(columns={'target_dif': 'diff'})
        # Assuming 'bop' column contains a mix of string and numeric values
        # Convert 'bop' column to numeric, coercing errors to NaN for non-numeric values
        result['bop'] = pd.to_numeric(result['bop'], errors='coerce')

        # Filter the DataFrame based on conditions
        result = result[(result['rel'] == 'X') | (result['bop'] > 0)]
        logging.info(result)

        # make car names smaller
        result['car_name'] = result['car_name'].replace(to_replace='Bentley Continental', value='Bentley')
        result['car_name'] = result['car_name'].replace(to_replace='McLaren 720S GT3 Evo', value='McLaren')
        result['car_name'] = result['car_name'].replace(to_replace='Mercedes-AMG GT3', value='Mercedes')
        result['car_name'] = result['car_name'].replace(to_replace='Audi R8 LMS GT3 evo II', value='Audi')
        result['car_name'] = result['car_name'].replace(to_replace='BMW M4 GT3', value='BMW')
        result['car_name'] = result['car_name'].replace(to_replace='Lamborghini Huracan GT3 EVO 2', value='Lambo')
        result['car_name'] = result['car_name'].replace(to_replace='Porsche 992 GT3 R', value='Porsche')
        result['car_name'] = result['car_name'].replace(to_replace='Ferrari 296 GT3', value='Ferrari')
        result['car_name'] = result['car_name'].replace(to_replace='AMR V8 Vantage', value='AMR V8')
        result['car_name'] = result['car_name'].replace(to_replace='Honda NSX GT3 Evo', value='Honda')
        result['car_name'] = result['car_name'].replace(to_replace='Ford Mustang GT3', value='Ford')
        result['car_name'] = result['car_name'].replace(to_replace='Nissan GT-R Nismo GT3', value='Nissan')

        # Assuming you have a pandas DataFrame named 'result'
        for index, row in result.iterrows():
            car_name = row['car_name']
            bop = f"{row['bop']}kg"
            
            # Add car name and BOP to the message
            message += f"{car_name}: {bop} — "

        message = f"{message} — Target Laptime: {target}"

        logging.info(message)
        await ctx.send(message)


    @commands.command(name='custombop') # type: ignore
    async def custombop(self, ctx):
        await ctx.send("We are checking, we are checking.")

        track = ctx.message.content.replace("!custombop ", "")

        logging.info(f"Track: {track}")

        track = converters.to_lfm_track_name(track)

        closest_track = BOP.find_closest_track(BOP.get_all_tracks(), track)

        result = BOP.get_completely_random_custom_bop(track)
        print(result)
        #result = result[(result['relevant'] == 'X') | (result['car_name'] == 'Ferrari 488 GT3 Evo') | (result['bop'] == 0)] 
        logging.info(result)

        # Initialize an empty message string
        message = f"{closest_track}: "

        # Remove the trailing " — " from the message
        #message = message[:-3]

        logging.info(track)
        result.drop("car_year", axis=1, inplace=True)
        result = result.rename(columns={'relevant': 'rel'})
        result = result.rename(columns={'bop_raw': 'raw'})
        result['target_dif'] = result['target_dif'].apply(lambda x: x[0] + x[4:] if x[1:3] == '00' else x)
        result = result.rename(columns={'target_dif': 'diff'})
        # Assuming 'bop' column contains a mix of string and numeric values
        # Convert 'bop' column to numeric, coercing errors to NaN for non-numeric values
        result['bop'] = pd.to_numeric(result['bop'], errors='coerce')

        # Filter the DataFrame based on conditions
        result = result[(result['rel'] == 'X') | (result['bop'] > 0)]
        logging.info(result)

        # make car names smaller
        result['car_name'] = result['car_name'].replace(to_replace='Bentley Continental', value='Bentley')
        result['car_name'] = result['car_name'].replace(to_replace='McLaren 720S GT3 Evo', value='McLaren')
        result['car_name'] = result['car_name'].replace(to_replace='Mercedes-AMG GT3', value='Mercedes')
        result['car_name'] = result['car_name'].replace(to_replace='Audi R8 LMS GT3 evo II', value='Audi')
        result['car_name'] = result['car_name'].replace(to_replace='BMW M4 GT3', value='BMW')
        result['car_name'] = result['car_name'].replace(to_replace='Lamborghini Huracan GT3 EVO 2', value='Lambo')
        result['car_name'] = result['car_name'].replace(to_replace='Porsche 992 GT3 R', value='Porsche')
        result['car_name'] = result['car_name'].replace(to_replace='Ferrari 296 GT3', value='Ferrari')
        result['car_name'] = result['car_name'].replace(to_replace='AMR V8 Vantage', value='AMR V8')
        result['car_name'] = result['car_name'].replace(to_replace='Honda NSX GT3 Evo', value='Honda')
        result['car_name'] = result['car_name'].replace(to_replace='Ford Mustang GT3', value='Ford')
        result['car_name'] = result['car_name'].replace(to_replace='Nissan GT-R Nismo GT3', value='Nissan')

        # Assuming you have a pandas DataFrame named 'result'
        for index, row in result.iterrows():
            car_name = row['car_name']
            bop = f"{row['bop']}kg"
            
            # Add car name and BOP to the message
            message += f"{car_name}: {bop} — "

        message = f"Here is your unique BOP good lady, or sir! — {message[:-2]} — powered by {ctx.author.name}"

        logging.info(message)
        await ctx.send(message)

    
    @commands.command(name='realbop') # type: ignore
    async def realbop(self, ctx):
        await ctx.send("We are checking, we are checking.")
        
         # Whatever should happen goes here for example:
        result = ""
        track = ctx.message.content.replace("!realbop ", "")

        logging.info(f"Track: {track}")

        track = converters.to_lfm_track_name(track)

        data = BOP.get_realbop(BOP.find_closest_track(BOP.get_all_tracks(), track))

        if data:
            cars = data['bop']['GT3']
            # Sort the cars by "bop" (ballast) in ascending order
            sorted_cars = sorted(cars, key=lambda x: x['ballast'])

            for car in sorted_cars:
                car_name = convert_car_names.convert_car_names(car['car_name'])
                ballast = f"{car['ballast']}kg"
                # Use string formatting to align car names
                result += (f'{car_name}: {ballast} — ')
            
        # Remove the trailing " — " from the message
        result = result[:-3]

        if data:
            bop_version = data['bop_version']
            active_since = data['active_since']

            result += (f' || [BOP Version {bop_version} — active since {active_since}]')
        
        await ctx.send(result)

    
    @commands.command(name='entrylist') # type: ignore
    async def entrylist(self, ctx):
         # Whatever should happen goes here for example:
        raceid, search = ctx.message.content.replace("!entrylist ", "").split(" ")
        raceid = raceid.replace("#", "")
        result = entrylist.get_entrylist(raceid, search)
        await ctx.send(result)


    @commands.command(name='proseries') # type: ignore
    async def proseries(self, ctx):
         # Whatever should happen goes here for example:
        input = ctx.message.content[len("!proseries "):]
        logging.info(f"!proseries args: {input}")

        if input == "all":
            await ctx.send("https://cdn.discordapp.com/attachments/1015611698348036096/1226414587583397928/vSSYxCSBvyyXkJqAhq3RzDYOl52B9vor3FClDG16GAk7rq7AsVRnyWpLJvXUocYfgwM8k.png?ex=662de917&is=661b7417&hm=397ad036e3ac8568fb6bb46bd054ab54856b339af220336495029aaf5ac1ca75&")
            return

        if input != '' and input.lstrip("+-").isnumeric():
            if input.startswith("+") or input.startswith("-"):
                weather = lfmproweather.getweather(None, int(input))
            else:
                weather = lfmproweather.getweather(int(input))
        else:
            weather = lfmproweather.getweather(None)

        track = weather['Track']
        degrees = weather['°C']
        hour = weather['Hour']
        eff_degrees = weather['Avg. °C']
        clouds = weather['Clouds']
        rain = weather['Rain']
        dry = weather['Dry']
        wet = weather['Wet']
        mixed = weather['Mixed']

        rain_emoji = emoji.emojize('🌧')
        sun_emoji = emoji.emojize('☀')

        # Create a combination of sun and rain cloud
        sun_with_rain_emoji = emoji.emojize('🌦')


        result = f'''
 {track} —
 {degrees} °C at {hour}:00, average ~{eff_degrees} °C —
 {sun_emoji} {dry} / {rain_emoji} {wet} / {sun_with_rain_emoji} {mixed}
'''
        await ctx.send(result)

        ##### BOP PART ####
        result = ""
        ##### ###  ### ####

        track = converters.to_lfm_track_name(track)

        data = BOP.get_realbop(BOP.find_closest_track(BOP.get_all_tracks(), track))

        if data:
            cars = data['bop']['GT3']
            # Sort the cars by "bop" (ballast) in ascending order
            sorted_cars = sorted(cars, key=lambda x: x['ballast'])

            for car in sorted_cars:
                car_name = convert_car_names.convert_car_names(car['car_name'])
                ballast = f"{car['ballast']}kg"
                # Use string formatting to align car names
                result += (f'{car_name}: {ballast} — ')
            
        # Remove the trailing " — " from the message
        result = result[:-3]

        if data:
            bop_version = data['bop_version']
            active_since = data['active_since']

            result += (f' || [BOP Version {bop_version} — active since {active_since}]')
        
        await ctx.send(result)

        if 5 <= datetime.today().isoweekday():  # Monday to Thursday
            await ctx.send("BOP can still change until Monday. DinkDonk")


    @commands.command(name='laprecords') # type: ignore
    async def laprecords(self, ctx):
    
        twitch_name = ctx.channel.name
        user_id = 0
        year = 0
        if "nordschleife" in ctx.message.content.lower():
            year = 2023
        args = ctx.message.content.replace("!laprecords ", "")
        try:
            year = re.findall(r'\b\d+\b', args)[0]
        except:
            year = 2024
        try:
            track, user_id = re.sub(r'\b\d+\b', '', args).strip().split(" - ")
        except:
            track = re.sub(r'\b\d+\b', '', args).strip()
            for user in users_list:
                if user["twitch"] == twitch_name:
                    user_id = user['id']
        
        if "nordschleife" in ctx.message.content.lower():
            year = 2023    
        rows = laprecords.get_laprecords(user_id, converters.to_lfm_track_name(track), year)
        
        if rows == None:
            await ctx.send("Didn't find no records.")
        else:
            for row in rows:
                await ctx.send(row)


    @commands.command(name='gt4laprecords') # type: ignore
    async def gt4laprecords(self, ctx):
    
        twitch_name = ctx.channel.name
        user_id = 0
        year = 0
        args = ctx.message.content.replace("!gt4laprecords ", "")
        try:
            year = re.findall(r'\b\d+\b', args)[0]
        except:
            year = 2024
        try:
            track, user_id = re.sub(r'\b\d+\b', '', args).strip().split(" - ")
        except:
            track = re.sub(r'\b\d+\b', '', args).strip()
            for user in users_list:
                if user["twitch"] == twitch_name:
                    user_id = user['id']
        
        if "nordschleife" in ctx.message.content.lower():
            year = 2023
        rows = laprecords.get_laprecords(user_id, converters.to_lfm_track_name(track), year, "GT4")
        
        if rows == None:
            await ctx.send("Didn't find no records.")
        else:
            for row in rows:
                await ctx.send(row)


    @commands.command(name='gt2laprecords') # type: ignore
    async def gt2laprecords(self, ctx):
    
        twitch_name = ctx.channel.name
        user_id = 0
        year = 0
        args = ctx.message.content.replace("!gt2laprecords ", "")
        
        try:
            year = re.findall(r'\b\d+\b', args)[0]
        except:
            year = 2024
        try:
            track, user_id = re.sub(r'\b\d+\b', '', args).strip().split(" - ")
        except:
            track = re.sub(r'\b\d+\b', '', args).strip()
            for user in users_list:
                if user["twitch"] == twitch_name:
                    user_id = user['id']
        
        if "nordschleife" in ctx.message.content.lower():
            year = 2023
        rows = laprecords.get_laprecords(user_id, converters.to_lfm_track_name(track), year, "GT2")
        
        if rows == None:
            await ctx.send("Didn't find no records.")
        else:
            for row in rows:
                await ctx.send(row)


    # Define the !stats command
    @commands.command(name='stats') # type: ignore
    async def stats(self, ctx):
        twitch_name = ctx.channel.name
        user_id = 0

        try:
            user_id = ctx.message.content.replace("!stats ", "")
            for user in users_list:
                if user["twitch"] == twitch_name:
                    user_id = user['id']
        except:
            user_id = ctx.message.content.replace("!stats ", "") # type: ignore

        logging.info(user_id)
            
        rows = userstats.get_stats(user_id)
        await ctx.send(rows)


    # Define the !target command
    @commands.command(name='target') # type: ignore
    async def targettime(self, ctx):
        await ctx.send("We are checking, we are checking.")
        
        track = ctx.message.content.replace("!target ","")
        track = converters.to_lfm_track_name(track)
        target = BOP.get_targettime(track)
        await ctx.send(f"Target for {track} is {target}.")


    @commands.command(name='bopcar') # type: ignore
    async def bopcar(self, ctx):
        if "help" in ctx.message.content:
            await ctx.send("!bopcar [track] - [car]")
            return
        
        await ctx.send("We are checking, we are checking.")
        
        track, car = ctx.message.content.replace("!bopcar ", "").split(" - ")

        logging.info(f"Track: {track} — Car: {car}")

        car = BOP.find_closest_car(car)
        track = converters.to_lfm_track_name(track)
        closest_track = BOP.find_closest_track(BOP.get_all_tracks(), track)

        logging.info(f"Track: {track} — Car: {car}")

        result = BOP.get_bop(track)
        print(result)
        if not result.empty:
            result = result[(result['car_name'] == car)]
            logging.info(result)
            result.reset_index(drop=True, inplace=True)

            logging.info(result)
            # Initialize an empty message string
            message = f"{closest_track}: "

            # Remove the trailing " — " from the message
            #message = message[:-3]

            target = BOP.get_targettime(track)
            message += f"{result['car_name'].to_string(index=False)} — "
            message += f"{result['lap'].to_string(index=False)} ({result['target_dif'].to_string(index=False)}) — "
            message += f"{result['bop_raw'].to_string(index=False)}kg"
            message += f" — Target Laptime: {target}"


            await ctx.send(message)
        else:
            ctx.send("LFM API didn't respond :(")


    @commands.command(name='elocalc') # type: ignore
    async def elocalc(self, ctx):
        if "help" in ctx.message.content.lower():
            await ctx.send("!elocalc raceid (with or without #) - complete name - position")
        
        parts = ctx.message.content.replace("!elocalc ", "").split(" - ")

        if len(parts) == 3:
            raceid, driver, position = parts[0], parts[1], parts[2]
        elif len(parts) == 2:
            raceid, position = parts[0], parts[1]
            for user in users_list:
                if user["twitch"] == ctx.channel.name:
                    driver = user['name']
        elif len(parts) == 1:
            position = parts[0]
            for user in users_list:
                if user["twitch"] == ctx.channel.name:
                    driver = user['name']
                    user_id = user['id']
                    raceid = self.drivers_races[user_id][0]

        logging.info("### ELO CALC ###")
        logging.info(f"{raceid}, {driver}, {position}")
        if "#" in raceid:
            raceid = raceid.replace("#","")
        result = elocalc.calc(int(raceid), driver, int(position))
        await ctx.send(result)


    @commands.command(name='penaltypoints') # type: ignore
    async def penaltypoints(self, ctx):
        if ctx.message.content == "!penaltypoints":
             await ctx.send(lfm_penaltypoints.get_list())
        else:
            driver = ctx.message.content.replace("!penaltypoints ", "")
            if driver == "help":
                await ctx.send("5s → 1 Pts || 10s → 1.5 Pts || 15s → 2 Pts || DT → 2.5 Pts || StopGo → 3 Pts")
            else:
                await ctx.send(lfm_penaltypoints.get_name(driver))
 
    @commands.command(name='search') # type: ignore
    async def search(self, ctx):
        try:
            query, items = ctx.message.content.replace(f"!{self.search.name} ","").split(" - ")
            logging.info(f"Query: {query} — Items: {items}")
            result = lfm_search.get_user_info(query, int(items))
            if int(items) > 1:
                for item in result:
                    await ctx.send(item)
                    await asyncio.sleep(1)
            else:
                await ctx.send(result)
        except:
            query = ctx.message.content.replace(f"!{self.search.name} ","")
            logging.info(f"Query: {query}")
            await ctx.send(lfm_search.get_user_info(query))


    @commands.command(name='weathercalc') # type: ignore
    async def weathercalc(self, ctx):
        rain, clouds, randomness, ambient, day, hour, duration = ctx.message.content.replace("!weathercalc ","").split(" - ")
        if int(ambient) < 12:
            ambient = 12
        if int(ambient) > 50:
            ambient = 50
        await ctx.send(f"Weather Simulation starting with: — "
            f"Rain: {rain} — "
            f"Clouds: {clouds} — "
            f"Randomness: {randomness} — "
            f"Temps: {ambient} — "
            f"Day: {day} & Hour {hour} — "
            f"Duration: {duration} minutes.")
        result = weathersim.run_simulations(float(rain), float(clouds), int(randomness), int(ambient), int(day), int(hour), int(duration))
        #user = ctx.user
        await ctx.send(result)


    @commands.command(name='nords') # type: ignore
    async def nords(self, ctx):
        await ctx.send("Nordschleife corner names: https://oversteer48.com/nurburgring-corner-names/")
        await ctx.send("Image: https://cdn.discordapp.com/attachments/1015611698348036096/1228320090903285801/Nurburgring_corner_map.jpg?ex=662b9d3a&is=6619283a&hm=d77cb04b8360adbbe430a4f83fc9b430a6bbd00bd40bad3665f2dda455b661a1&")


    @commands.command(name='accdown') # type: ignore
    async def accdown(self, ctx):
        url = "https://api2.lowfuelmotorsport.com/api/accstatus"
        request = requests.get(url)
        if request.status_code != 200:
            logging.error(
                f"Failed to execute request {url}, {request.status_code}: {request.text}"
            )
            status = None
        else:
            status = request.json()

        if status is None:
            await ctx.send("Can't find acc status. :(")
        else:   
            text = status.get("text")
            if "offline" in text.lower():
                await ctx.send("ACC Servers are DOWN")
            else:
                await ctx.send("ACC Servers are UP")


    @commands.command(name='accstatus') # type: ignore
    async def accstatus(self, ctx):
        url = "https://api2.lowfuelmotorsport.com/api/accstatus"
        request = requests.get(url)
        if request.status_code != 200:
            logging.error(
                f"Failed to execute request {url}, {request.status_code}: {request.text}"
            )
            status = None
        else:
            status = request.json()

        if status is None:
            await ctx.send("Can't find acc status. :(")
        else:   
            text = status.get("text")
            if "offline" in text.lower():
                await ctx.send("ACC Servers are DOWN")
            else:
                await ctx.send("ACC Servers are UP")


    @commands.command(name='mph') # type: ignore
    async def mph(self, ctx):
        input = ctx.message.content.replace("!mph ", "")
        await ctx.send(str(speedconverter.mph_to_kph(input)))


    @commands.command(name='kmh') # type: ignore
    async def kmh(self, ctx):
        input = ctx.message.content.replace("!kmh ", "")
        await ctx.send(str(speedconverter.kph_to_mph(input)))


    @commands.command(name='livetiming') # type: ignore
    async def livetiming(self, ctx):
        await ctx.send("LFM Livetiming: https://community.accsolutions.de/accdrive/ 👍")


    # Define the !leaderboard command
    @commands.command(name='leaderboard') # type: ignore
    async def leaderboard(self, ctx):
        twitch_name = ctx.channel.name
        user_id = 0

        for user in users_list:
            if user["twitch"] == twitch_name:
                user_id = user['id']
        
        sort = ctx.message.content.replace("!leaderboard ", "") # type: ignore

        logging.info(user_id)
        
        result = user_stats_comparison.get_stats(sort, user_id)
        await ctx.send(result)


    # Define the !leaderboard command
    @commands.command(name='lfmlaprecords') # type: ignore
    async def lfm_laprecords(self, ctx):
        if "help" in ctx.message.content:
            await ctx.send(f"!{self.lfm_laprecords.name} [track] [R/Q]")
        content = ctx.message.content.replace(f"!{self.lfm_laprecords.name} ","")

        print(content)
        
        try:
            track, session = content.lower().split(" ")
        except:
            await ctx.send("track and q/r please. I'm guessing you meant q.")
            track, session = (content, "q")

        track = converters.to_lfm_track_name(track)
        track_id = laprecords.get_track_id(track, 2024)

        if session == "q":
            result = lfm_laprecords.get_qualifying(track_id)
            await ctx.send(f"{track.title()} Qualifying: {result}")
        elif session == "r":
            result = lfm_laprecords.get_race(track_id)
            await ctx.send(f"{track.title()} Race: {result}")
        else:
            await ctx.send("!laprecords [track] [R/Q]")

    # Define the !weather command
    @commands.command(name='weather') # type: ignore
    async def reallifeweather(self, ctx):
        if "help" in ctx.message.content:
            await ctx.send(f"!{self.reallifeweather.name} searchquery")
        query = ctx.message.content.replace(f"!{self.reallifeweather.name} ","")

        await ctx.send(weather.get_weather_query(query))


    # Define the !offseason command
    @commands.command(name='offseason') # type: ignore
    async def offseason(self, ctx):
    
        gt2_m2 = "Kyalami—Ricardo Tormo—Suzuka—Monza—Barcelona"
        gt3_gt4 = "Spielberg—Paul Ricard—Nürburgring—Silverstone—Spa"
        legacy_cup = "Watkins Glen—Zandvoort—Misano—Hungaroring—Bathurst"


        def get_offseason(s: str) -> str:
            # Split the string into a list of items
            items = s.split('—')

            # Get the current day of the week (0 is Monday, 6 is Sunday)
            day_of_week = datetime.today().weekday()

            # If today is between Monday (0) and Friday (4), add [] around the corresponding item
            if day_of_week < 5:
                # Modify the item based on the current day
                items[day_of_week] = f"[{items[day_of_week]}]"

            # Join the items back into a string with '—'
            result = '—'.join(items)

            return result
        
        await ctx.send(f"GT2 + M2 Multiclass: {get_offseason(gt2_m2)}")
        await ctx.send(f"GT3 + GT4 Multiclass: {get_offseason(gt3_gt4)}")
        await ctx.send(f"Legacy Duo Cup Multiclass: {get_offseason(legacy_cup)}")
        await ctx.send("Did you know you can sub for free with Prime Gaming? Link your Amazon Prime and grab a free sub every month — no cost to you, just a little gift from Jeffi Bezos! 😎")
        

    # Define the !weather command
    @commands.command(name='entered_races') # type: ignore
    async def entered_races(self, ctx):
        twitch_name = ctx.channel.name
        user_id = 0

        for user in users_list:
            if user["twitch"] == twitch_name:
                user_id = user['id']
                
        result = (", ".join(map(str, self.drivers_races[user_id])))
        await ctx.send(f"Entered, but not completed Races for Streamer: {result}")

if __name__ == "__main__":
    bot = TwitchBot()
    bot.run()