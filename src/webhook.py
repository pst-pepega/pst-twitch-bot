import requests
import json
from dotenv import load_dotenv
import os
import logging

# Configure the logger
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

# Load environment variables from .env file
load_dotenv()
ITB = os.getenv('ITB')
SDL = os.getenv('SDL')

def send_message_itb(message, webhook_url = ITB) -> str:
    data = {
        "content": message
    }

    headers = {
        "Content-Type": "application/json"
    }

    response = requests.post(webhook_url, data=json.dumps(data), headers=headers) # type: ignore

    if response.status_code == 204:
        logging.info("Message sent successfully!")
        return "Thank you for your contribution."
    else:
        logging.info(f"Failed to send message. Status code: {response.status_code}")
        logging.info(response.text)
        return "It's broken 😭"
    

def send_message_sdl(message, webhook_url = SDL) -> str:
    data = {
        "content": message
    }

    headers = {
        "Content-Type": "application/json"
    }

    response = requests.post(webhook_url, data=json.dumps(data), headers=headers) # type: ignore

    if response.status_code == 204:
        logging.info("Message sent successfully!")
        return "Thank you for your contribution."
    else:
        logging.info(f"Failed to send message. Status code: {response.status_code}")
        logging.info(response.text)
        return "It's broken 😭"