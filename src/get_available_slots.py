import logging
import requests

# Configure logging
#logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

# Define constants
URL = "https://api3.lowfuelmotorsport.com/api/race/172062"
CAR_CLASS = "GT3"

def get_available_slots():
    try:
        # Fetch the API data
        response = requests.get(URL)
        response.raise_for_status()  # Raise an error for bad status codes
        data = response.json()


        # Step 2: Count participants with car_class == CAR_CLASS
        splits = data["splits"]["participants"]
        slots_taken = 0

        logging.info(splits)

        for participant in splits:  
            entries = participant['entries']
            logging.info(entries)
            slots_taken += sum(1 for entry in entries if entry.get("car_class") == CAR_CLASS)

        logging.info(slots_taken)
        # Step 3: Get max slots for the car class
        class_settings = data.get("event", {}).get("settings", {}).get("season_event_settings", {}).get("class_settings", [])
        logging.info(class_settings)
        max_slots = class_settings.get(CAR_CLASS, {}).get("max_slots", 0)

        # Step 4: Calculate available slots
        available_slots = max_slots - slots_taken

        return available_slots

    except requests.RequestException as e:
        print(f"Error fetching data: {e}")
        return None
    except KeyError as e:
        print(f"Unexpected data structure: missing key {e}")
        return None

# Call the function and print the result
available_slots = get_available_slots()
if available_slots is not None:
    print(f"Number of available slots for {CAR_CLASS}: {available_slots}")
