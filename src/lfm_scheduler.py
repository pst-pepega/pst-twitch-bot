import json
from datetime import datetime, timedelta
import logging


def load_lfm_data():
    with open("resources/json/lfm_schedule.json") as f:
        lfm_data = json.load(f)
    return lfm_data

def find_track_by_input(input_text):
    lfm_data = load_lfm_data()
    matching_tracks = []

    for track in range(len(lfm_data)):
      if lfm_data[track]['track']:
        if input_text.lower() in lfm_data[track]["track"].lower():
            date = lfm_data[track]["date"]
            series = lfm_data[track]["series"]
            matching_tracks.append(f"{date} — {series}")

    return matching_tracks

def find_next_week():
    lfm_data = load_lfm_data()
    next_week_data = []

    # Calculate the start and end dates for next week
    today = datetime.today()
    next_week_start = (today + timedelta(days=(7 - today.weekday()))).replace(hour=0, minute=0, second=0, microsecond=0)
    next_week_end = (next_week_start + timedelta(days=6)).replace(hour=0, minute=0, second=0, microsecond=0)

    print(today)
    print(next_week_start)
    print(next_week_end)

    # Append items to the list if their date falls within the next week's range
    for item in lfm_data:
        item_date = datetime.strptime(item['date'], '%d/%m/%Y')
        logging.info(f"Item date: {item_date}")
        if next_week_start <= item_date <= next_week_end:
            if item['track'] != "None":
                next_week_data.append(item)
                print(f"Item date: {item_date} added to next week data")

    return next_week_data

def find_current_week():
    lfm_data = load_lfm_data()
    next_week_data = []

    # Calculate the start and end dates for next week
    today = datetime.today()
    next_week_start = (today + timedelta(days=(7 - today.weekday()))).replace(hour=0, minute=0, second=0, microsecond=0) + timedelta(-7)
    next_week_end = (next_week_start + timedelta(days=6)).replace(hour=0, minute=0, second=0, microsecond=0)

    print(next_week_end)

    # Append items to the list if their date falls within the next week's range
    for item in lfm_data:
        item_date = datetime.strptime(item['date'], '%d/%m/%Y')
        if next_week_start <= item_date <= next_week_end:
            if item['track'] != "None":
                next_week_data.append(item)

    return next_week_data

# Example usage:
# brand = input("Enter the brand name (e.g., 'Aston Martin'): ")
# matching_cars = find_cars_by_brand(brand)

# if matching_cars:
#    print(f"Matching cars for '{brand}':")
#    for car in matching_cars:
#        print(f"- Model: {car['Model']}, Steering lock: {car['Steering lock']}")
#else:
#    print(f"No cars found for '{brand}'.")
