import math

def split_fuel_string(fuel_string):
	# Initialize the output variables
	minutes = None
	fuel_per_lap = None
	lap_time = None

	# Split the string by spaces and remove the first element (the "!fuel" part)
	fuel_list = fuel_string.split()[1:]

	# Loop through the remaining elements and assign them to the output variables based on their format
	for element in fuel_list:
		# If the element is an integer, it is the minutes
		if element.isdigit():
			minutes = int(element)
			# If the element contains a dot, it is the fuel per lap
		elif "." in element and not ":" in element:
			fuel_per_lap = float(element)
			# If the element contains a colon, it is the lap time
		elif ":" in element:
			lap_time = element

	# Return the output variables as a tuple
	return (minutes, fuel_per_lap, lap_time)


def fuel_calc(duration: int, fuel_per_lap: float, laptime: str):
	#print("boob")
	fuel_per_lap = float(fuel_per_lap)
	duration = round(float(duration), 2)
	# Convert laptime to seconds
	laptime_temp = laptime.split(":")
	# Split to minutes and seconds
	laptime_minutes = int(laptime_temp[0])
	laptime_temp2 = laptime_temp[1].split(".")
	# Split to seconds and milliseconds
	laptime_seconds = int(laptime_temp2[0])
	if(len(laptime_temp2)) == 1:
		laptime_milliseconds = 0
	else:
		laptime_milliseconds = int(laptime_temp2[1])

	if len(str(laptime_milliseconds)) == 1:
		laptime_milliseconds *= 100
	elif len(str(laptime_milliseconds)) == 2:
		laptime_milliseconds *= 10

	#print(f"{laptime_minutes}:{laptime_seconds}.{laptime_milliseconds}")
	laptime = laptime_minutes*60 + laptime_seconds + laptime_milliseconds*0.001

	if laptime_milliseconds == 0:
		laptime_milliseconds = "000"

	# Convert duration to seconds
	duration_seconds = duration * 60
	
	# Calculate the number of laps needed (rounded up)
	#print(f"{duration_seconds}/{laptime}")
	laps_needed = math.ceil(duration_seconds / laptime)
	#print(laps_needed)
	last_lap = round((duration_seconds - laps_needed * laptime) * -1, 3)
	#print(last_lap)
	
	# Calculate total fuel required
	total_fuel = round(float(laps_needed * fuel_per_lap), 3)
	formation_lap = round(total_fuel + 1.5 * fuel_per_lap, 3)
	
	message = f"For a {duration} minute race with a {laptime_minutes}:{laptime_seconds}.{laptime_milliseconds} lap time \u2500 Total Fuel: {total_fuel} liters \u2500 Fuel (incl. formation lap): {formation_lap} liters \u2500 Bonus Lap Margin: -{last_lap} seconds (excluding pit stops)"
	#print(f"For a {duration} minute race with a {laptime_minutes}:{laptime_seconds}.{laptime_milliseconds} lap time \u2500 Total Fuel: {total_fuel} liters \u2500 Fuel (incl. formation lap): {formation_lap} liters \u2500 Bonus Lap Margin: -{last_lap} seconds (excluding pit stops)")


	# Return the message and total_fuel in a list
	return [str(f"{total_fuel} [{laps_needed} Laps]"), message]

# Example usage
duration_minutes = 60
fuel_per_lap = 2.5
laptime = "1:42.5"

#result = fuel_calc(duration_minutes, fuel_per_lap, laptime)
##print(result)
