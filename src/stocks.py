import yfinance as yf

def get_data(stock_symbol):
    # Fetch stock data
    stock_data = yf.Ticker(stock_symbol)

    # Get historical stock data
    historical_data = stock_data.history(period="5d")  # Fetch data for the last 5 days

    # Check if there is enough data to calculate change percentage
    if len(historical_data) >= 2:
        closing_price = historical_data['Close'].iloc[-1]
        previous_closing_price = historical_data['Close'].iloc[-2]
        change_percentage = ((closing_price - previous_closing_price) / previous_closing_price) * 100

        # Format and print the data
        formatted_data = f"{closing_price:.3f} [{change_percentage:.2f}%]"
        print(formatted_data)
        return(formatted_data)
    else:
        return None
        print("Not enough historical data to calculate change percentage.")
