import requests
import pandas as pd
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import logging
import json
import datetime
import numpy as np

logger = logging.getLogger(__name__)
LFM_API = "https://api2.lowfuelmotorsport.com/api"


def get_track_id(track_name):

    track_list_response = requests.get(f"{LFM_API}/lists/getTracks")

    # Check if the request was successful (status code 200)
    if track_list_response.status_code != 200:
        logger.warn("Failed to get list of track IDs from LFM API")
        return None

    data = track_list_response.json()

    # Filter tracks with track_id between 124 and 155
    filtered_tracks = [track for track in data if 124 <= track['track_id'] <= 155 or track['track_id'] == 223 or track['track_id'] == 249]

    # Create a dictionary with track_name as keys and track_id as values
    track_dict = {track['track_name']: track['track_id'] for track in filtered_tracks}

    # Convert track_dict keys to a list of strings
    track_list = list(track_dict.keys())

    # Find the closest matching track name
    closest_track = find_closest_track(track_list, track_name)

    # Get the corresponding track_id
    track_id = track_dict.get(closest_track)

    return track_id


def find_closest_track(match_tracks, input_track):
    closest_match, _ = process.extractOne(input_track, match_tracks, scorer=fuzz.partial_ratio)
    return closest_match


def get_all_tracks():

    # Make a GET request to the API
    response = requests.get(f"{LFM_API}/lists/getTracks")

    # Check if the request was successful (status code 200)
    if response.status_code != 200:
        logger.warning("Failed to get track ID from LFM API")
        return None

    # Parse the JSON response
    data = response.json()

    # Filter tracks with track_id between 124 and 155
    filtered_tracks = [track for track in data if 124 <= track['track_id'] <= 155 or track['track_id'] == 223 or track['track_id'] == 249]

    # Create a dictionary with track_name as keys and track_id as values
    track_dict = {track['track_name']: track['track_id'] for track in filtered_tracks}

    # Convert track_dict keys to a list of strings
    track_list = list(track_dict.keys())

    return track_list


def get_bop(track):
        
    # URL to fetch JSON data from
    url = f'https://api2.lowfuelmotorsport.com/api/hotlaps/getBopPrediction?track={get_track_id(track)}'
    logger.info(url)
    try:
        # Send an HTTP GET request to the URL and parse the JSON response
        data = requests.get(url).json()

        # Check if the JSON data has the expected structure
        if 'laps_relevant' in data and 'laps_others' in data:
            relevant_laps = data['laps_relevant']
            others_laps = data['laps_others']

            # Convert the relevant laps data to a Pandas DataFrame
            relevant_df = pd.DataFrame(relevant_laps)
            # Add a "relevant" column and set it to True
            relevant_df['relevant'] = 'X'

            # Convert the others laps data to a Pandas DataFrame
            others_df = pd.DataFrame(others_laps)
            # Add a "relevant" column and set it to an empty string
            others_df['relevant'] = ''

            # Sort the others DataFrame by "bop_raw" in descending order
            others_df = others_df.sort_values(by='bop_raw', ascending=False)

            # Combine both DataFrames into one
            combined_df = pd.concat([relevant_df, others_df])
            combined_df = combined_df.sort_values(by='bop_raw', ascending=False)

            # Reorder columns
            combined_df = combined_df[['car_name', 'car_year', 'target_dif', 'lap', 'bop', 'bop_raw', 'relevant']]
            # Reset the row numbering to start from 1
            combined_df.reset_index(drop=True, inplace=True)
            # Add 1 to the index to start from 1
            combined_df.index += 1

            # Print the combined DataFrame
            return combined_df
        else:
            print('JSON data does not have the expected structure.')
    except Exception as e:
        print(f'An error occurred: {e}')


def get_completely_random_custom_bop(track):
        
    # URL to fetch JSON data from
    url = f'https://api2.lowfuelmotorsport.com/api/hotlaps/getBopPrediction?track={get_track_id(track)}'
    logger.info(url)
    try:
        # Send an HTTP GET request to the URL and parse the JSON response
        data = requests.get(url).json()

        # Check if the JSON data has the expected structure
        if 'laps_relevant' in data and 'laps_others' in data:
            relevant_laps = data['laps_relevant']
            others_laps = data['laps_others']

            # Convert the relevant laps data to a Pandas DataFrame
            relevant_df = pd.DataFrame(relevant_laps)
            # Add a "relevant" column and set it to True
            relevant_df['relevant'] = 'X'

            # Convert the others laps data to a Pandas DataFrame
            others_df = pd.DataFrame(others_laps)
            # Add a "relevant" column and set it to an empty string
            others_df['relevant'] = ''

            # Sort the others DataFrame by "bop_raw" in descending order
            others_df = others_df.sort_values(by='bop_raw', ascending=False)

            # Combine both DataFrames into one
            combined_df = pd.concat([relevant_df, others_df])
            combined_df = combined_df.sort_values(by='bop_raw', ascending=False)

            # Reorder columns
            combined_df = combined_df[['car_name', 'car_year', 'target_dif', 'lap', 'bop', 'bop_raw', 'relevant']]
            # Reset the row numbering to start from 1
            combined_df.reset_index(drop=True, inplace=True)
            # Add 1 to the index to start from 1
            combined_df.index += 1

            combined_df['bop'] = pd.to_numeric(combined_df['bop'])
            combined_df['bop'] = combined_df['bop'] + np.random.randint(-5, 6, size=len(combined_df))
            combined_df = combined_df.sort_values(by='bop', ascending=False)

            # Print the combined DataFrame
            return combined_df
        else:
            print('JSON data does not have the expected structure.')
    except Exception as e:
        print(f'An error occurred: {e}')


def get_all_bop():
    # Initialize an empty DataFrame to store the combined results
    combined_df = pd.DataFrame()

    # Assuming you have a list of tracks
    track_list = get_all_tracks()

    # Iterate through the track_list
    for i, track in enumerate(track_list):
        # Calculate the bop for the current track
        df1 = get_bop(track)

        # Calculate the bop for the next track, if available
        #if i < len(track_list) - 1:
        #    next_track = track_list[i + 1]
        #    print(f"Next track: {next_track}")
        #    df2 = get_bop(next_track)
        #else:
            # If there is no next track, create an empty DataFrame
            #df2 = pd.DataFrame()

        # Concatenate the data for the current track and the next track
        combined_track_df = pd.concat([df1, combined_df])
        combined_df = combined_track_df.groupby(['car_name', 'car_year', 'relevant'], as_index=False)['bop_raw'].sum()
        #print(f"{track}:")
        #print(combined_df)
        #print("-----")

        # Append the combined data for the current track to the combined DataFrame
        #combined_df = pd.concat([combined_df, combined_track_df], ignore_index=True)

    # Group by car_name and car_year and calculate the sum of bop_raw
    #grouped = combined_df.groupby(['car_name', 'car_year', 'relevant'])['bop_raw'].sum().reset_index()

    grouped = combined_df
    # Calculate the bop as described (limited to -40 and 40)
    grouped['bop'] = (grouped['bop_raw'] / 23).clip(lower=-40, upper=40)
    grouped['bop_raw'] = (grouped['bop_raw'] / 23)
    grouped = grouped[['car_name', 'car_year', 'bop', 'bop_raw', 'relevant']]

    # Drop the bop_raw column
    combined_df.drop(columns=['bop_raw'], inplace=True)            

    # Convert 'bop' column to numeric
    grouped['bop'] = pd.to_numeric(grouped['bop'], errors='coerce')
    grouped['bop'] = grouped['bop'].round(2)


    # Convert 'bop' column to numeric
    grouped['bop_raw'] = pd.to_numeric(grouped['bop_raw'], errors='coerce')
    grouped['bop_raw'] = grouped['bop_raw'].round(2)


    # Sort by 'bop' column
    grouped = grouped.sort_values(by='bop_raw', ascending=False)

    # Reset the index
    grouped.reset_index(drop=True, inplace=True)
    grouped.index += 1

    # Now grouped contains the sum of bop_raw values for each car across all tracks
    return grouped


def get_json(track):
    # URL to fetch JSON data from
    url = f'https://api2.lowfuelmotorsport.com/api/hotlaps/getBopPrediction?track={get_track_id(track)}'
    try:
        # Send an HTTP GET request to the URL and parse the JSON response
        data = requests.get(url).json()
        
        track = get_acc_track_name(track)
        track = track[:-5]

        output_json = {
            "entries": []
        }

        # Iterate through laps_relevant and laps_others
        for entry in data['laps_relevant'] + data['laps_others']:
            output_entry = {
                "track": track,  # You can set the track name here
                "carModel": entry['car_id'],
                "ballastKg": int(entry['bop'])
            }
            output_json["entries"].append(output_entry)

        # Convert the result to JSON format
        result_json = json.dumps(output_json, indent=4)

        # Write the JSON data to a file
        filename = f"bop.json"
        with open(filename, "w") as file:
            file.write(result_json)

        return filename

    except Exception as e:
        print(f'An error occurred: {e}')


def get_acc_track_name(track_name):

    track_list_response = requests.get(f"{LFM_API}/lists/getTracks")

    # Check if the request was successful (status code 200)
    if track_list_response.status_code != 200:
        logger.warn("Failed to get list of track IDs from LFM API")
        return None

    data = track_list_response.json()

    # Filter tracks with track_id between 124 and 155
    filtered_tracks = [track for track in data if 124 <= track['track_id'] <= 155 or track['track_id'] == 223 or track['track_id'] == 249]

    # Create a dictionary with track_name as keys and track_id as values
    track_dict = {track['track_name']: track['acc_track_name'] for track in filtered_tracks}

    # Convert track_dict keys to a list of strings
    track_list = list(track_dict.keys())

    # Find the closest matching track name
    closest_track = find_closest_acc_track(track_list, track_name)

    # Get the corresponding track_id
    track_id = track_dict.get(closest_track)

    return track_id


def find_closest_acc_track(match_tracks, input_track):
    closest_match, _ = process.extractOne(input_track, match_tracks, scorer=fuzz.partial_ratio)
    return closest_match


def get_all_bop_tsv():
    # Initialize an empty list to store DataFrames
    dfs = []

    # List of tracks
    track_list = get_all_tracks()

    # Loop through the tracks
    for track in track_list:
        # Replace this line with your get_bop(track) function to get the data
        # For demonstration, we'll create a sample DataFrame
        data = get_bop(track)
        
        # Create a DataFrame from the sample data
        df = pd.DataFrame(data)
        
        # Add the track name as a column
        df['track'] = track
        
        # Append the DataFrame to the list
        dfs.append(df)

    # Concatenate the DataFrames
    result_df = pd.concat(dfs, ignore_index=True)

    df = pd.DataFrame(result_df)

    # Pivot the DataFrame
    pivot_df = df.pivot(index=['car_name', 'car_year'], columns=['track'], values='bop')

    # Replace NaN values with "—"
    pivot_df = pivot_df.fillna('—')

    # Flatten the multi-level columns
    # pivot_df.columns = [f'{col[0]}_{col[1]}' for col in pivot_df.columns]

    # Reset index
    pivot_df.reset_index(inplace=True)

    # Display the result
    return pivot_df


def get_realbop(input_track):
    # Send a GET request to the URL to fetch the JSON data
    url = 'https://api2.lowfuelmotorsport.com/api/hotlaps/getAccBop'

    response = requests.get(url)
    data = response.json()

    # Iterate through the tracks in the JSON data
    for track in data:
        if track['track_name'] == input_track:
            return track  # Return the data for the specified track

    # If the track is not found, return None or raise an exception
    return None  # or raise an exception with a custom message


def get_targettime(track):

    # URL to fetch JSON data from
    url = f'https://api2.lowfuelmotorsport.com/api/hotlaps/getBopPrediction?track={get_track_id(track)}'
    logger.info(url)
    try:
        # Send an HTTP GET request to the URL and parse the JSON response
        data = requests.get(url).json()
        kgtime = data['bopdata']['target_time']
        return kgtime

    except Exception as e:
        print(f'An error occurred: {e}')


def get_cars():

    response = requests.get(f"{LFM_API}/lists/getCars")
    if response.status_code != 200:
        logger.warn("Failed to get list of cars from LFM API")
        return None

    data = response.json()

    # Filter the data based on the 'car_id' condition
    filtered_cars = [car for car in data if car['car_id'] <= 1000]

    # Custom sorting function
    def custom_sort(car):
        # Sort by class first
        class_order = {'GT3': 0, 'GT4': 1}
        class_value = car['class']
        class_priority = class_order.get(class_value, 2)  # Default to 2 for other classes

        # Sort by year and then car name
        return (class_priority, -int(car['year']), car['car_name'])

    # Sort the filtered list using the custom_sort function
    sorted_cars = sorted(filtered_cars, key=custom_sort)

    # Extract car names from the sorted list
    car_names = [car['car_name'] for car in sorted_cars]

    return car_names


def find_closest_car(input_car, match_car = get_cars()):
    closest_match, _ = process.extractOne(input_car, match_car, scorer=fuzz.partial_ratio)
    return closest_match
