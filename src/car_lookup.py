
def Car_lookup(car_id):
    if(car_id == 0):
        return "Porsche 991 GT3 R"
    if(car_id == 1):
        return "Mercedes-AMG GT3 2015"
    if(car_id == 2):
        return "Ferrari 488 GT3"
    if(car_id == 3):
        return "Audi R8 LMS"
    if(car_id == 4):
        return "Lamborghini Huracan GT3"
    if(car_id == 5):
        return "McLaren 650S GT3"
    if(car_id == 6):
        return "Nissan GT-R Nismo GT3 2018"
    if(car_id == 7):
        return "BMW M6 GT3"
    if(car_id == 8):
        return "Bentley Continental 2018"
    if(car_id == 9):
        return "Porsche Cup 2017"
    if(car_id == 10):
        return "Nissan GT-R Nismo GT3 2015"
    if(car_id == 11):
        return "Bentley Continental 2015"
    if(car_id == 12):
        return "Aston Martin V12 Vantage GT3"
    if(car_id == 13):
        return "Reiter Engineering R-EX GT3"
    if(car_id == 14):
        return "Emil Frey Jaguar G3"
    if(car_id == 15):
        return "Lexus RC F GT3"
    if(car_id == 16):
        return "Lamborghini Huracan GT3 Evo"
    if(car_id == 17):
        return "Honda NSX GT3"
    if(car_id == 18):
        return "Lambo ST"
    if(car_id == 19):
        return "Audi R8 LMS Evo"
    if(car_id == 20):
        return "AMR V8 Vantage GT3"
    if(car_id == 21):
        return "Honda NSX GT3 Evo"
    if(car_id == 22):
        return "McLaren 720S GT3"
    if(car_id == 23):
        return "Porsche 991II GT3 R"
    if(car_id == 24):
        return "Ferrari 488 GT3 Evo"
    if(car_id == 25):
        return "Mercedes-AMG GT3 2020"
    if(car_id == 26):
        return "Ferrari Challenge"
    if(car_id == 27):
        return "BMW M2 CS Racing"
    if(car_id == 28):
        return "Porsche 911 GT3 Cup (Type 992)"
    if(car_id == 29):
        return "Lambo ST Evo 2"
    if(car_id == 30):
        return "BMW M4 GT3"
    if(car_id == 31):
        return "Audi R8 LMS evo II"
    if(car_id == 32):
        return "Ferrari 296 GT3"
    if(car_id == 33):
        return "Lamborghini Huracan GT3 EVO 2"
    if(car_id == 34):
        return "Porsche 992 GT3 R"
    if(car_id == 35):
        return "McLaren 720S GT3 Evo"
    if(car_id == 36):
        return "Ford Mustang GT3"
    if(car_id == 50):
        return "Alpine A110 GT4"
    if(car_id == 51):
        return "Aston Martin Vantage GT4"
    if(car_id == 52):
        return "Audi R8 LMS GT4"
    if(car_id == 53):
        return "BMW M4 GT4"
    if(car_id == 55):
        return "Chevrolet Camaro GT4"
    if(car_id == 56):
        return "Ginetta G55 GT4"
    if(car_id == 57):
        return "KTM X-Bow GT4"
    if(car_id == 58):
        return "Maserati MC GT4"
    if(car_id == 59):
        return "McLaren 570S GT4"
    if(car_id == 60):
        return "Mercedes AMG GT4"
    if(car_id == 61):
        return "Porsche 718 Cayman Cayman GT4 Clubsport"
    if(car_id == 83):
        return "Maserati GT2"
    if(car_id == 84):
        return "Mercedes AMG GT2"
    if(car_id == 85):
        return "Porsche 911 GT2 RS CS Evo"
    if(car_id == 80):
        return "Audi R8 LMS (GT2)"
    if(car_id == 82):
        return "KTM XBOW GT2"
    if(car_id == 86):
        return "Porsche 935"
    return ""