import requests
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import logging
from typing import Optional

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
LFM_API = "https://api2.lowfuelmotorsport.com/api"


def get_track_id(track_name, year: int) -> int:
    logger.info(f"get_track_id({track_name}, {year})")
    year = int(year)
    track_list_response = requests.get(f"{LFM_API}/lists/getTracks")

    # Check if the request was successful (status code 200)
    if track_list_response.status_code != 200:
        logger.warn("Failed to get list of track IDs from LFM API")
        return None
    
    #logger.info(year)
    #logger.info(year == 2023)
    data = track_list_response.json()
    filtered_tracks = []
    if year == 2023:
        # Filter tracks with track_id between 124 and 155
        filtered_tracks = [track for track in data if 124 <= track['track_id'] <= 155 or track['track_id'] == 223 or track['track_id'] == 249]
        #logger.info("2023 Tracks")
    elif year == 2024:
        filtered_tracks = [track for track in data if 223 <= track['track_id'] <= 246 or track['track_id'] <= 249]
        #logger.info("2024 Tracks")
    
    #logger.info(filtered_tracks)

    # Create a dictionary with track_name as keys and track_id as values
    track_dict = {track['track_name']: track['track_id'] for track in filtered_tracks}

    # Convert track_dict keys to a list of strings
    track_list = list(track_dict.keys())

    # Find the closest matching track name
    closest_track = find_closest_track(track_list, track_name)

    # Get the corresponding track_id
    track_id = track_dict.get(closest_track)

    return track_id # type: ignore


def find_closest_track(match_tracks, input_track):
    closest_match, _ = process.extractOne(input_track, match_tracks, scorer=fuzz.partial_ratio)
    return closest_match


def get_laprecords(user_id: int, track_name: str, year: int = 2024, car_class: str = "GT3") -> Optional[list]:

    url = f"{LFM_API}/users/getUserTrackRecords/{user_id}/{car_class}"
    logger.info(url)
    results = ["", ""]

    response = requests.get(url)

    if response.status_code == 200:
        data = response.json()

        track_id_to_find = get_track_id(track_name, year)  # Replace with the track_id you're looking for
        logger.info(track_id_to_find)

        try:
            for record in data:
                if record.get("track_id") == track_id_to_find:
                    logger.info(record.get("track_id"))
                    logger.info(record)
                    qualifying_record = record["records"]["qualifying"]
                    race_record = record["records"]["race"]

                    results[0] = f"Qualifying: {qualifying_record['lap']} — {qualifying_record['car_name']} [{qualifying_record['date']}]"
                    results[1] = f"Race: {race_record['lap']} — {race_record['car_name']} [{race_record['date']}]"
                    return results
        except:
                return None
    else:
        print(f"Failed to fetch data. Status code: {response.status_code}")

# result = get_laprecords(1704, "Monza")

# if result != None:
#     for row in result:
#         print(row)
# else:
#     print("Didn't find any records.")