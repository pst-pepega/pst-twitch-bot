import requests

def fetch_most_active_data():
    url = "https://api2.lowfuelmotorsport.com/api/statistics/mostActive"
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        return data
    else:
        return None

def format_difference(value):
    return f" (Δ{value})"

def most_active_command(position):
    data = fetch_most_active_data()
    if data is None or not data:
        return "Failed to fetch data from the API. Please try again later."

    max_users = len(data)
    if not 1 <= position <= max_users:
        return f"Invalid position. Please provide a valid position between 1 and {max_users}."

    user = data[position - 1]
    user_1 = data[position - 2]

    separator = " ─ "  # Use a Unicode horizontal bar as the separator

    message = f"{position}. {user['vorname']} {user['nachname']}{separator}{user['races']} races {separator}{user['laps']} laps {separator}{user['km']} kilometres"

    if position != 1:
        races_diff = (user['races'] - user_1['races'])*-1
        laps_diff = (user['laps'] - user_1['laps'])*-1
        km_diff = (round(float(user['km']) - float(user_1['km']), 3))*-1
        message = f"{position}. {user['vorname']} {user['nachname']}{separator}{user['races']} races{format_difference(races_diff)}{separator}{user['laps']} laps{format_difference(laps_diff)}{separator}{user['km']} kilometers{format_difference(km_diff)}"

    return message

def find_user_position(name):
    print(f"trying to find {name}")
    data = fetch_most_active_data()
    if data is None or not data:
        return None

    for i, user in enumerate(data):
        full_name = f"{user['vorname']} {user['nachname']}"
        if name.lower() == full_name.lower():
            return i + 1  # Position in the data starts from 1

    return None

def check_with_id(id):
    # URLs of the JSON data
    user_favourite_cars_url = f"https://api2.lowfuelmotorsport.com/api/users/getUsersFavouriteCars/{id}"
    most_active_url = "https://api2.lowfuelmotorsport.com/api/statistics/mostActive"

    # Fetch the JSON data for user favourite cars
    response_favourite = requests.get(user_favourite_cars_url)
    data_favourite = response_favourite.json()

    # Fetch the JSON data for most active users
    response_most_active = requests.get(most_active_url)
    data_most_active = response_most_active.json()

    # Initialize counters
    total_laps = 0
    total_km = 0.0

    # Loop through the data and calculate totals for user favourite cars
    for car_data in data_favourite:
        total_laps += car_data["laps"]
        total_km += float(car_data["km"])

    # Get the 99th value's kilometers and laps from most active users
    value_99_kms = data_most_active[98]["km"]
    value_99_laps = data_most_active[98]["laps"]

    # Calculate the differences
    difference_kms = (total_km - float(value_99_kms))*-1
    difference_laps = (total_laps - value_99_laps)*-1

    # Print the differences
    if difference_kms < 0:
        return "You are already in the top 100, run command again with your name."
    else:
        return f"{difference_kms:.4f} kilometres until you are in the top 100 most active users on LFM. [And {difference_laps} laps.]"


# Example usage:
# Assuming you want the data for the second user, call the command like this:
#print(most_active_command(1))
#print(most_active_command(2))
#print(most_active_command(3))
#position = find_user_position("Bruhier Maedre")
#print(position)
#print(most_active_command(position))

