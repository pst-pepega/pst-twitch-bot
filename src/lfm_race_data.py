import json
import urllib.error
import urllib.request
from datetime import datetime
from dateutil import tz
import emoji
import weathersim

def timestamp(time_string: str) -> int:
	# Convert the string to a datetime object
	date_time_obj = datetime.strptime(time_string, "%Y-%m-%d %H:%M:%S")

	# Convert the datetime object to a Discord timestamp format
	return int(date_time_obj.timestamp()-3600)


def get_race(race_id):
	url = f"https://api2.lowfuelmotorsport.com/api/race/{race_id}"
	response = urllib.request.urlopen(url)
	data = json.loads(response.read())
	date_and_time = data['race_date']
	eff_ambient = 0.0
        
	track = data['track']['track_name']
	ambient = data['server_settings']['server_settings']['event']['data']['ambientTemp']
	clouds = data['server_settings']['server_settings']['event']['data']['cloudLevel']
	rain = data['server_settings']['server_settings']['event']['data']['rain']
	randomness = data['server_settings']['server_settings']['event']['data']['weatherRandomness']
	hour = data['server_settings']['server_settings']['event']['data']['sessions'][2]['hourOfDay']
	duration = int(data['server_settings']['server_settings']['event']['data']['sessions'][2]['sessionDurationMinutes']) + int(data['server_settings']['server_settings']['event']['data']['sessions'][1]['sessionDurationMinutes']) + 5
	day = data['server_settings']['server_settings']['event']['data']['sessions'][2]['dayOfWeekend']

	race_info = f"{track} @ {date_and_time} UTC+1 — {ambient}°C @ {hour}:00 with {int(clouds*100)}% {emoji.emojize(':cloud:')} — {int(rain*100)}% {emoji.emojize(':cloud_with_rain:')} [🎲 {randomness}]"
	result = weathersim.run_simulations(float(rain), float(clouds), int(randomness), int(ambient), int(day), int(hour), int(duration))

	return (race_info, result)