from fuzzywuzzy import fuzz, process
import json

def find_closest_track(track_name, json_file = "resources/json/pitlane.json"):
    # Load the JSON data
    with open(json_file, 'r') as file:
        data = json.load(file)

    # Extract track names
    track_names = [track['name'] for track in data['tracks']]

    # Find the closest match
    closest_match, score = process.extractOne(track_name, track_names)

    # Find the corresponding track data
    for track in data['tracks']:
        if track['name'] == closest_match:
            return track