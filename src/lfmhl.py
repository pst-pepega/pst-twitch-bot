import datetime
from typing import Optional
import requests
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import logging

logger = logging.getLogger(__name__)
LFM_API = "https://api2.lowfuelmotorsport.com/api"


def format_time(seconds: int | float) -> str:
    time = datetime.time(second=seconds)
    return time.strftime("%H:%S:%fff")

    minutes, seconds = divmod(seconds, 60)
    seconds, milliseconds = divmod(seconds, 1)
    milliseconds = int(milliseconds * 1000)
    return f"{int(minutes):02d}:{int(seconds):02d}:{milliseconds:03d}"


def get_hotlaps(track_name: str) -> Optional[dict]:
    hotlap_url = f"{LFM_API}/hotlaps?track={get_track_id(track_name)}&major=1.10&version="
    logger.info(hotlap_url)
    response = requests.get(hotlap_url)

    if response.status_code == 200:
        hotlaps_data = response.json()
        # print(hotlaps_data['data'])
        return hotlaps_data['data']
    else:
        # print(f"Failed to retrieve hotlaps data for {track_name}. Status code: {response.status_code}")
        return None


def get_track_id(track_name):

    track_list_response = requests.get(f"{LFM_API}/lists/getTracks")

    # Check if the request was successful (status code 200)
    if track_list_response.status_code != 200:
        logger.warn("Failed to get list of track IDs from LFM API")
        return None

    data = track_list_response.json()

    # Filter tracks with track_id between 124 and 155
    filtered_tracks = [track for track in data if 124 <= track['track_id'] <= 155 or track['track_id'] == 249]

    # Create a dictionary with track_name as keys and track_id as values
    track_dict = {track['track_name']: track['track_id'] for track in filtered_tracks}

    # Convert track_dict keys to a list of strings
    track_list = list(track_dict.keys())

    # Find the closest matching track name
    closest_track = find_closest_track(track_list, track_name)

    # Get the corresponding track_id
    track_id = track_dict.get(closest_track)

    logger.info(track_id)
    return track_id


def get_cars():

    response = requests.get(f"{LFM_API}/lists/getCars")
    if response.status_code != 200:
        logger.warn("Failed to get list of cars from LFM API")
        return None

    data = response.json()

    # Filter the data based on the 'car_id' condition
    filtered_cars = [car for car in data if car['car_id'] <= 1000]

    # Custom sorting function
    def custom_sort(car):
        # Sort by class first
        class_order = {'GT3': 0, 'GT4': 1}
        class_value = car['class']
        class_priority = class_order.get(class_value, 2)  # Default to 2 for other classes

        # Sort by year and then car name
        return (class_priority, -int(car['year']), car['car_name'])

    # Sort the filtered list using the custom_sort function
    sorted_cars = sorted(filtered_cars, key=custom_sort)

    # Extract car names from the sorted list
    car_names = [car['car_name'] for car in sorted_cars]

    return car_names


def find_closest_track(match_tracks, input_track):
    closest_match, _ = process.extractOne(input_track, match_tracks, scorer=fuzz.partial_ratio)
    return closest_match


def find_closest_car(match_car, input_car):
    closest_match, _ = process.extractOne(input_car, match_car, scorer=fuzz.partial_token_sort_ratio)
    return closest_match


def find_hotlaps_for_driver(hotlaps_data, driver_name, acc_patch):
    matching_hotlaps = []

    for hotlap in hotlaps_data:
        if acc_patch == "":
            if f'{hotlap["vorname"].lower()} {hotlap["nachname"].lower()}' == driver_name.lower():
                matching_hotlaps.append(hotlap)
        else:
            if f'{hotlap["vorname"].lower()} {hotlap["nachname"].lower()}' == driver_name.lower():
                return hotlap

    return matching_hotlaps


def find_hotlaps_for_driver_and_car(hotlaps_data, driver_name, car_name, acc_patch):
    matching_hotlaps = []

    logger.info(f"{driver_name} - {car_name} - {acc_patch}")

    for hotlap in hotlaps_data:
        if acc_patch == "":
            if f'{hotlap["vorname"].lower()} {hotlap["nachname"].lower()}' == driver_name.lower() and hotlap["car_name"].lower() == car_name.lower():
                matching_hotlaps.append(hotlap)
        else:
            if f'{hotlap["vorname"].lower()} {hotlap["nachname"].lower()}' == driver_name.lower():
                return hotlap

    return matching_hotlaps


def find_hotlaps_for_car(hotlaps_data, car_name, acc_patch):
    matching_hotlaps = []

    for hotlap in hotlaps_data:
        if acc_patch == "":
            if hotlap["car_name"].lower() == car_name.lower():
                matching_hotlaps.append(hotlap)
        else:
            if hotlap["car_name"].lower() == car_name.lower():
                return hotlap

    return matching_hotlaps


def get_data(input_track, driver_name, input_car, acc_patch):

    car_list = get_cars()

    closest_track = input_track

    if input_car != "":
        closest_car = find_closest_car(car_list, input_car)

    hotlaps_data = get_hotlaps(closest_track)

    if input_car == "" and driver_name.lower() != "none":
        matching_hotlaps = find_hotlaps_for_driver(hotlaps_data, driver_name, acc_patch)

    elif driver_name.lower() == "none" and input_car != "":
        matching_hotlaps = find_hotlaps_for_car(hotlaps_data, closest_car, acc_patch)
        matching_hotlaps = matching_hotlaps[:5]

    else:
        matching_hotlaps = find_hotlaps_for_driver_and_car(hotlaps_data, driver_name, closest_car, acc_patch)

    return matching_hotlaps


# Function to process hotlaps data
def car_records(track):
    hotlaps_data = get_hotlaps(track)

    # Dictionary to store unique tracks and their first result for each car
    unique_cars = {}

    for hotlap in hotlaps_data:
        car_name = hotlap['car_name']

        # If track_name is not in the unique_tracks dictionary, add it with the car_name and hotlap data
        if car_name not in unique_cars:
            unique_cars[car_name] = f"{hotlap['lap']}" 

    return unique_cars


def get_all_track_results_for_user(driver_name):

    # Make a GET request to the API
    response = requests.get(f"{LFM_API}/lists/getTracks")

    # Check if the request was successful (status code 200)
    if response.status_code != 200:
        logger.warning("Failed to get track list from LFM API")
        return None

    # Parse the JSON response
    data = response.json()

    # Filter tracks with track_id between 124 and 155
    filtered_tracks = [track for track in data if 124 <= track['track_id'] <= 155 or track['track_id'] == 249]

    # Create a dictionary with track_name as keys and track_id as values
    track_dict = {track['track_name']: track['track_id'] for track in filtered_tracks}

    # Convert track_dict keys to a list of strings
    track_list = list(track_dict.keys())

    print(track_list)

    # Initialize an empty dictionary to store the laps for the driver
    lap_list = {}
    
    for track in track_list:
        hotlaps = get_hotlaps(track)
        for hotlap in hotlaps:
            driver_full_name = f'{hotlap["vorname"].lower()} {hotlap["nachname"].lower()}'

            if driver_full_name.lower() == driver_name.lower():
                # Check if the track is already in lap_list
                if track not in lap_list:
                    lap_list[track] = []

                # Add the hotlap to the list for this track
                lap_list[track].append(hotlap)

    # Now lap_list contains all laps for the specified driver
    print("Lap List")
    print(lap_list)
    return lap_list



def get_user_track_records(driver_name):

    # Make a GET request to the API
    response = requests.get(f"{LFM_API}/lists/getTracks")

    # Check if the request was successful (status code 200)
    if response.status_code != 200:
        logger.warning("Failed to get track list from LFM API")
        return None

    # Parse the JSON response
    data = response.json()

    # Filter tracks with track_id between 124 and 155
    filtered_tracks = [track for track in data if 123 <= track['track_id'] <= 155 or track['track_id'] == 249]

    # Create a dictionary with track_name as keys and track_id as values
    track_dict = {track['track_name']: track['track_id'] for track in filtered_tracks}

    # Convert track_dict keys to a list of strings
    track_list = list(track_dict.keys())

    print(track_list)

    # Initialize an empty dictionary to store the laps for the driver
    lap_list = {}

    for track in track_list:
        hotlaps = get_hotlaps(track)
        for hotlap in hotlaps:
            driver_full_name = f'{hotlap["vorname"].lower()} {hotlap["nachname"].lower()}'

            if driver_full_name.lower() == driver_name.lower():
                # Found a result for the driver, add it to the lap_list dictionary
                lap_list[track] = hotlap  # Assuming 'track' is a suitable key for the lap
                break  # Exit the inner loop since you've found the driver's lap

    # Now lap_list contains the laps for the specified driver
    print("Lap List")
    print(lap_list)
    return lap_list


def get_all_tracks():

    # Make a GET request to the API
    response = requests.get(f"{LFM_API}/lists/getTracks")

    # Check if the request was successful (status code 200)
    if response.status_code != 200:
        logger.warning("Failed to get track ID from LFM API")
        return None

    # Parse the JSON response
    data = response.json()

    # Filter tracks with track_id between 124 and 155
    filtered_tracks = [track for track in data if 123 <= track['track_id'] <= 155 or track['track_id'] == 249]

    # Create a dictionary with track_name as keys and track_id as values
    track_dict = {track['track_name']: track['track_id'] for track in filtered_tracks}

    # Convert track_dict keys to a list of strings
    track_list = list(track_dict.keys())

    return track_list

def world_record(track):
    data = get_hotlaps(track)
    data = data[:1]
    logger.info(data)
    return data[0]


def main():

    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

    input_track = "Hungaroring"
    input_car = "Lambo"
    driver_name = "None"

    hotlaps_data = get_hotlaps(input_track)

    if not hotlaps_data:
        logger.error(f"No hotlap data found for driver {driver_name} on track {input_track}")
        return

    matching_hotlaps = get_data(input_track, driver_name, input_car, "")

    for hotlaps in matching_hotlaps:
        logger.info(f"{hotlaps['vorname']} {hotlaps['nachname']} - {hotlaps['car_name']} - {hotlaps['lap']} - {hotlaps['version']} - {hotlaps['lapdate']} UTC")


if __name__ == "__main__":
    main()
