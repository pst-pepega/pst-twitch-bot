import logging

logger = logging.getLogger(__name__)


def to_lfm_track_name(track_name: str) -> str:

    track_name = track_name.lower()

    match track_name:
        case "imola":
            return "Enzo"

        case "cota":
            return "americas"

        case "bathurst":
            return "panorama"

        case "valencia":
            return "tormo"

        case "barcelona" | "barca":
            return "catalunya"

        case "rbr":
            return "spielberg"
        
        case "nords":
            return "nordschleife"
        
        case "nurbs":
            return "nürburgring"
        
        case "nbr":
            return "nürburgring"
        
        case _:
            return track_name