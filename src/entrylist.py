import requests

def Car_lookup(car_id):
    if(car_id == 0):
        return "Porsche 2018"
    if(car_id == 1):
        return "Mercedes 2015"
    if(car_id == 2):
        return "Ferrari"
    if(car_id == 3):
        return "Audi 2015"
    if(car_id == 4):
        return "Lambo 2015"
    if(car_id == 5):
        return "McLaren 650S"
    if(car_id == 6):
        return "Nissan 2018"
    if(car_id == 7):
        return "BMW M6"
    if(car_id == 8):
        return "Bentley 2018"
    if(car_id == 9):
        return "Porsche Cup 2017"
    if(car_id == 10):
        return "Nissan 2015"
    if(car_id == 11):
        return "Bentley 2015"
    if(car_id == 12):
        return "Aston Martin V12"
    if(car_id == 13):
        return "Reiter"
    if(car_id == 14):
        return "Jaguar"
    if(car_id == 15):
        return "Lexus"
    if(car_id == 16):
        return "Lambo Evo"
    if(car_id == 17):
        return "Honda"
    if(car_id == 18):
        return "Lambo ST"
    if(car_id == 19):
        return "Audi Evo"
    if(car_id == 20):
        return "Aston Martin V8"
    if(car_id == 21):
        return "Honda Evo"
    if(car_id == 22):
        return "McLaren 720S"
    if(car_id == 23):
        return "Porsche 2019"
    if(car_id == 24):
        return "Ferrari Evo"
    if(car_id == 25):
        return "Mercedes 2020"
    if(car_id == 26):
        return "Ferrari Challenge"
    if(car_id == 27):
        return "BMW M2"
    if(car_id == 28):
        return "Porsche Cup 2021"
    if(car_id == 29):
        return "Lambo ST Evo 2"
    if(car_id == 30):
        return "BMW M4"
    if(car_id == 31):
        return "Audi Evo 2"
    if(car_id == 32):
        return "Ferrari 296"
    if(car_id == 33):
        return "Lamborghini Evo 2"
    if(car_id == 34):
        return "Porsche 992"
    if(car_id == 35):
        return "McLaren 720S Evo"
    if(car_id == 50):
        return "Alpine"
    if(car_id == 51):
        return "Aston Martin GT4"
    if(car_id == 52):
        return "Audi GT4"
    if(car_id == 53):
        return "BMW GT4"
    if(car_id == 55):
        return "Chevrolet"
    if(car_id == 56):
        return "Ginetta"
    if(car_id == 57):
        return "KTM"
    if(car_id == 58):
        return "Maserati"
    if(car_id == 59):
        return "McLaren 570S"
    if(car_id == 60):
        return "Mercedes GT4"
    if(car_id == 61):
        return "Porsche Cayman"
    return ""

def get_entrylist(raceid, search):
    result = ""
    # Define the URL
    url = f"https://api2.lowfuelmotorsport.com/api/race/{raceid}"  # Replace with the actual URL

    # Make an HTTP GET request to the URL
    response = requests.get(url)

    # Check if the request was successful (status code 200)
    if response.status_code == 200:
        # Parse the JSON response
        data = response.json()

        # Check if "entrylist" exists in the JSON
        if "entrylist" in data and "entries" in data["entrylist"]:
            entries = data["entrylist"]["entries"]
            i = 1

            # Iterate through the entries and print the desired information
            for entry in entries:
                drivers = entry.get("drivers", [])
                for driver in drivers:
                    first_name = driver.get("firstName", "N/A")
                    last_name = driver.get("lastName", "N/A")
                    name = f"{first_name} {last_name}"
                    number = f"[{i}]" 
                    forced_car_model = Car_lookup(entry.get("forcedCarModel", "N/A"))
                    if search in name:
                        result += (f"{number} — {name} — {forced_car_model}")
                        return result
                    i += 1
                    
            return "Did not find your driver :("

        else:
            print("No 'entrylist' or 'entries' found in the JSON response.")
    else:
        print("Failed to retrieve data. Status code:", response.status_code)