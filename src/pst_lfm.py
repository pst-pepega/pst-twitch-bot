import json
import requests
import logging
from typing import List, Any
from datetime import datetime
from dateutil import tz
from typing import Optional
import csv
import os

logger = logging.getLogger(__name__)
LFM_API = "https://api2.lowfuelmotorsport.com/api"
CSV_FILE = 'resources/drivers.csv'
USERS_FILE = 'resources/json/users_list.json'

def load_user_list(file_path = USERS_FILE):
    """
    Load the user list from a JSON file.
    
    :param file_path: The path to the JSON file.
    :return: A list of users.
    """
    with open(file_path, 'r') as file:
        user_list = json.load(file)
    return user_list


def get_row(criteria_key, criteria_value: str):
    """
    Get a row from the CSV file based on a criteria.
    
    :param criteria_key: The column name to search for (driver_name, lfmid, twitch, last_race).
    :param criteria_value: The value to search for in the specified column.
    :return: The matching row as a dictionary, or None if no match is found.
    """
    with open(CSV_FILE, mode='r', newline='') as file:
        reader = csv.DictReader(file)
        for row in reader:
            if str(row[criteria_key]) == str(criteria_value):
                return row
    return None


def set_row(criteria_key, criteria_value, new_data):
    """
    Set a row in the CSV file based on a criteria. If the row is found, it updates it with new data.
    If the row is not found, it appends the new data as a new row.
    
    :param criteria_key: The column name to search for (driver_name, lfmid, twitch, last_race).
    :param criteria_value: The value to search for in the specified column.
    :param new_data: A dictionary with the new row data.
    """
    rows = []
    row_updated = False
    
    # Read the existing data
    with open(CSV_FILE, mode='r', newline='') as file:
        reader = csv.DictReader(file)
        for row in reader:
            if row[criteria_key] == str(criteria_value):
                rows.append(new_data)
                row_updated = True
            else:
                rows.append(row)
    
    # If the row was not found, append the new data
    if not row_updated:
        rows.append(new_data)

    os.remove(CSV_FILE)
    
    # Write the data back to the file
    with open(CSV_FILE, mode='w', newline='') as file:
        writer = csv.DictWriter(file, fieldnames=reader.fieldnames)
        writer.writeheader()
        writer.writerows(rows)


def get_users_past_races(user_id: int) -> List[dict[str, Any]]:

    url = f"{LFM_API}/users/getUsersPastRaces/{user_id}?start=0&limit=5"
    response = requests.get(url)
    return response.json()


def get_race_time(race_data: dict) -> datetime:

    race_date = datetime.strptime(race_data["race_date"], "%Y-%m-%d %H:%M:%S")

    race_date = race_date.replace(tzinfo=tz.tzstr("UTC+1"))
    race_date_local = race_date.astimezone(tz.tzlocal())

    return race_date_local


def rating_change_converter(rating: int) -> str:

    match rating:
        case rating if rating > 0:
            return "📈"
        case rating if rating < 0:
            return "📉"
        case _:
            return "∆"
        

async def check_last_race(lfm_id, name, twitch) -> Optional[List[str]]:
    message = []

    past_races = get_users_past_races(lfm_id)

    if len(past_races) == 0:
        logger.info(f"LFM user {lfm_id} never raced")
        return None, 0, 0, 0

    last_race = past_races[0]
    
    new_race_id = last_race.get("race_id")

    criteria_key = 'lfm_id'
    criteria_value = lfm_id

    
    # Get a row based on the criteria
    row = get_row(criteria_key, criteria_value)
    if row:
        print(row)
        if str(row['last_race']) == str(new_race_id):
            return None, 0, 0, 0
    
    # Define new data
    new_data = {
        'driver_name': name,
        'lfm_id': lfm_id,
        'twitch': twitch,
        'last_race': new_race_id
    }

    set_row(criteria_key, criteria_value, new_data)

    users_data = requests.get(f"{LFM_API}/users/getUserData/{lfm_id}").json()
    user_fullname = f"{users_data.get('vorname')} {users_data.get('nachname')}"
    position_delta = last_race['start_pos'] - last_race['finishing_pos']
    sr_change_diff = rating_change_converter(last_race.get("sr_change"))
    elo_change_diff = rating_change_converter(last_race.get("rating_change"))
    position_change_diff = rating_change_converter(position_delta)

    message.append(f"""
    Latest LFM race for {user_fullname}, 
    {last_race['event_name']} in the {last_race['car']} at {last_race['track_name']}
    {position_change_diff} P{last_race['start_pos']} -> P{last_race['finishing_pos']} [{position_delta}], 
    {elo_change_diff} {last_race['rating_change']} Elo, 
    {sr_change_diff} {last_race['sr_change']} SR [{last_race['incidents']} IP]
""")

    data = requests.get(f"{LFM_API}/users/getUserStats/{lfm_id}").json()
    podiums = data["podium"]
    wins = data["wins"]
    poles = data["poles"]

    if last_race['finishing_pos'] == 1:
        message.append((f"Congratulations on your {get_ordinal(wins)} victory! Well done!"))

    elif last_race['finishing_pos'] <= 3:
        message.append(f"You've clinched a spot on the podium! Way to go, your {get_ordinal(podiums)}, congrats!")
        message.append(f"Technically your {get_ordinal(podiums+wins)}... But yea.")

    if last_race['start_pos'] == 1:
        message.append(f"{get_ordinal(poles)} Pole Position, Let's go!")

    if last_race['incidents'] == 0:
        message.append("0 Incident Points. Very nice.")

    return message, float(last_race['sr_change']), int(last_race['rating_change']), new_race_id


async def set_last_race(lfm_id, name, twitch) -> Optional[str]:
    past_races = get_users_past_races(lfm_id)

    if len(past_races) == 0:
        logger.info(f"LFM user {lfm_id} never raced")
        return None

    last_race = past_races[0]
    
    new_race_id = last_race.get("race_id")

    criteria_key = 'lfm_id'
    criteria_value = lfm_id

    
    # Get a row based on the criteria
    row = get_row(criteria_key, criteria_value)
    if row:
        if str(row['last_race']) == str(new_race_id):
            return
    
    # Define new data
    new_data = {
        'driver_name': name,
        'lfm_id': lfm_id,
        'twitch': twitch,
        'last_race': new_race_id
    }

    set_row(criteria_key, criteria_value, new_data)

    return new_race_id


def get_ordinal(number: int) -> str:
    if 10 <= number % 100 <= 20:
        suffix = "th"
    else:
        suffix = {1: "st", 2: "nd", 3: "rd"}.get(number % 10, "th")

    return str(number) + suffix