import math
import random
import threading
from queue import Queue
import logging

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)

class WeatherSim:
    def __init__(self, rain_level_config, cloud_level_config, randomness, ambient_temp_config):
        self.rain_level_config = rain_level_config
        self.cloud_level_config = cloud_level_config
        self.ambient_temp_config = ambient_temp_config
        self.randomness = max(0, min(randomness, 7))

        # Factor used for scaling the randomness
        randomness_factor = self.randomness / 7

        # How many passes the noise function does
        self.noise_passes = 0
        if self.randomness > 0:
            self.noise_passes = math.floor(randomness_factor * 10) + 4

        # Generates the noise coefficient array
        self.noise_coefficients = []
        if self.randomness > 0:
            variance = randomness_factor * 0.4
            r = self._constrain(self._rand_normal_dist(0, variance), -2 * variance, 2 * variance)
            self.noise_coefficients.append(r * 0.2)
            for i in range(1, self.noise_passes):
                r = self._constrain(self._rand_normal_dist(0, variance), -2 * variance, 2 * variance)
                self.noise_coefficients.append((2.5 / self.noise_passes) * r)

        # Other coefficients
        self.other_coefficients = [self._rand_normal_dist(self._constrain(0, -1.6, 1.6), 1.3)]

        # Base temperature for temperature calculation
        self.base_temp = ambient_temp_config + 1 - 2 * random.random() * randomness_factor


    def calculate_weather(self, weektime):
        result = {
            'noise': 0,
            'rain_level': self.rain_level_config,
            'cloud_level': self.cloud_level_config,
            'brightness': 0,
            'ambient_temp': 0,
            'track_temp': 0,
        }

        if self.noise_passes == 0:
            # Skip rain and cloud calculation
            result['noise'] = self.rain_level_config
            result['rain_level'] = self.rain_level_config
            result['cloud_level'] = self.cloud_level_config
        else:
            # Calculate noise function
            noise = self.rain_level_config
            noise += weektime / 86400 * self.noise_coefficients[0]
            for i in range(1, self.noise_passes):
                s = 0
                s = math.sin(weektime * 3.14156 / 86400 + self.other_coefficients[0])
                s = math.sin((s * 86400 / 3.14156 + weektime) * i * 2 * 3.14156 * 2 / 86400)
                noise += s * self.noise_coefficients[i]

            result['noise'] = noise

            # Cloud level is cloud level config + noise
            cloud_level = noise + self.cloud_level_config
            result['cloud_level'] = self._constrain(cloud_level, 0, 1)

            # Rain is only allowed to exist if the cloud level is at least 0.6
            # If the rain level is zero and the randomness is less than four,
            # rain is always zero.
            result['rain_level'] = 0
            if (self.rain_level_config > 0 or self.randomness > 3) and result['cloud_level'] >= 0.6:
                rain_level = (cloud_level - 0.6) * 1.4875 + 0.15
                rain_level = self._constrain(rain_level, 0, noise)
                result['rain_level'] = self._constrain(rain_level, 0, 1)

        # Brightness is a cosine with a period of one day (86400 seconds)
        # and offset by two hours (7200 seconds)
        result['brightness'] = -0.2 - math.cos((weektime - 7200) / 86400 * 3.14156 * 2)

        # Hotness factor goes up with increasing temperature.
        # Hotness factor is equal to one when the temperature is 25 degrees.
        hotness_factor = self.base_temp / 25

        # Ambient temperature
        result['ambient_temp'] = self.base_temp + (6 - result['cloud_level'] * 3) * result['brightness'] - \
                                (result['rain_level'] * 4 + result['cloud_level']) * hotness_factor

        # Track temperature
        brightness_clipped = max(result['brightness'], 0)
        result['track_temp'] = result['ambient_temp'] + \
                               (result['ambient_temp'] + 20) * (0.25 - 0.125 * (result['cloud_level'] + result['rain_level']) * hotness_factor) * brightness_clipped

        return result


    def set_seeds(self, noise_coefficients, other_coefficients, base_temp):
        self.noise_coefficients = noise_coefficients
        self.other_coefficients = other_coefficients
        self.base_temp = base_temp


    def set_rain_level_config(self, new_rain_level_config):
        self.rain_level_config = new_rain_level_config


    def set_cloud_level_config(self, new_cloud_level_config):
        self.cloud_level_config = new_cloud_level_config


    def set_ambient_temp_config(self, new_ambient_temp_config):
        self.ambient_temp_config = new_ambient_temp_config
        variance = self.randomness / 7
        self.base_temp = self.ambient_temp_config + 1 - random.random() * 2 * variance


    def get_noise_coefficients(self):
        return self.noise_coefficients


    def get_other_coefficients(self):
        return self.other_coefficients


    def get_base_temp(self):
        return self.base_temp


    def _constrain(self, value, minimum, maximum):
        return max(minimum, min(value, maximum))


    def _rand_normal_dist(self, base_value, variance):
        a, b, c = 0, 0, 2
        while c > 1:
            a = random.uniform(-1, 1)
            b = random.uniform(-1, 1)
            c = a * a + b * b

        aa = math.sqrt(math.log(c) * -2 / c) * a
        return base_value + variance * aa
    

    def simulate_weather_duration(self, start_time, duration, results_queue):
        current_time = start_time
        wet_count = 0
        dry_count = 0

        start_temps = []
        end_temps = []
        max_temps = []
        min_temps = []
        avg_temps = []

        for _ in range(duration + 1):
            weather_result = self.calculate_weather(current_time)

            if weather_result['rain_level'] > 0:
                wet_count += 1
            else:
                dry_count += 1

            start_temps.append(weather_result['ambient_temp'])
            end_temps.append(weather_result['ambient_temp'])
            max_temps.append(weather_result['ambient_temp'])
            min_temps.append(weather_result['ambient_temp'])
            avg_temps.append(weather_result['ambient_temp'])

            current_time += 60  # Assuming each iteration is 5 minutes (300 seconds)

        results_queue.put({
            'weather_condition': 'mixed' if wet_count > 0 and dry_count > 0 else 'wet' if wet_count > 0 else 'dry',
            'start_temp': start_temps[0],
            'end_temp': end_temps[-1],
            'max_temp': max(max_temps),
            'min_temp': min(min_temps),
            'avg_temp': sum(avg_temps) / len(avg_temps),
            'wet_count': wet_count,
            'dry_count': dry_count
        })


def run_simulation(simulation_count, rain, clouds, randomness, ambient, start, duration):
    results_queue = Queue()
    threads = []
    return_string = ""

    # Set the start time and duration for which you want to simulate the weather (in seconds)
    start_time = start  # day 2, hour 17
    simulation_duration = duration  # 60 minutes

    for simulation_number in range(1, simulation_count + 1):
        # Create a new instance of WeatherSim for each simulation
        weather_sim = WeatherSim(rain_level_config=rain, cloud_level_config=clouds, randomness=randomness, ambient_temp_config=ambient)

        thread = threading.Thread(target=weather_sim.simulate_weather_duration, args=(start_time, simulation_duration, results_queue))
        threads.append(thread)
        thread.start()

        #print(f"Simulation {simulation_number} started.")

    for thread in threads:
        thread.join()

    # Process results
    wet_count = dry_count = mixed_count = 0
    avg_start_temp = avg_end_temp = avg_max_temp = avg_min_temp = avg_avg_temp = 0

    while not results_queue.empty():
        result = results_queue.get()

        if result['weather_condition'] == 'wet':
            wet_count += 1
        elif result['weather_condition'] == 'dry':
            dry_count += 1
        elif result['weather_condition'] == 'mixed':
            mixed_count += 1

        avg_start_temp += result.get('start_temp', 0)
        avg_end_temp += result.get('end_temp', 0)
        avg_max_temp += result.get('max_temp', 0)
        avg_min_temp += result.get('min_temp', 0)
        avg_avg_temp += result.get('avg_temp', 0)

        #print(f"Simulation {result['weather_condition']} | Wet Count: {result['wet_count']} | Dry Count: {result['dry_count']}")

    avg_start_temp /= simulation_count
    avg_end_temp /= simulation_count
    avg_max_temp /= simulation_count
    avg_min_temp /= simulation_count
    avg_avg_temp /= simulation_count

    return_string += f"Wet: {round(wet_count / simulation_count * 100, 1)}% — "
    return_string += f"Dry: {round(dry_count / simulation_count * 100, 1)}% — "
    return_string += f"Mixed: {round(mixed_count / simulation_count * 100, 1)}% — "
    #return_string += f"Avg Start Temp: {round(avg_start_temp,3)} — "
    #return_string += f"Avg End Temp: {round(avg_end_temp,3)} — "
    #return_string += f"Avg Max Temp: {round(avg_max_temp,3)} — "
    #return_string += f"Avg Min Temp: {round(avg_min_temp,3)} — "
    #return_string += f"Avg Avg Temp: {round(avg_avg_temp,3)}"
    return_string += f"Avg Temp: {round(avg_avg_temp,3)}"

    return return_string

# Run 1000 simulations
def run_simulations(rain, clouds, randomness, ambient, day, hour, duration):
    logging.info("Simulation started with:\n"
        f"Rain: {rain}\n"
        f"Clouds: {clouds}\n"
        f"Randomness: {randomness}\n"
        f"Temps: {ambient}\n"
        f"Day: {day} & Hour {hour}\n"
        f"Duration: {duration} minutes\n")
    result = run_simulation(1000, rain, clouds, randomness, ambient, ((day-1)*24+hour*3600), duration)

    return result