#!/bin/bash

python_script="src/main.py"

while :
do
    # Run the Python script in the background, redirecting both stdout and stderr to a file
    python3.11 "$python_script" 2>&1 | tee output.log &

    # Capture the process ID of the Python script
    python_pid=$!

    # Monitor the output for "Websocket closed"
    tail -f output.log | while read -r line
    do
        if [[ "$line" == *"Websocket connection was closed"* ]]; then
            echo "Detected 'Websocket closed' in the output. Restarting..."
            # Kill the Python script
            kill $python_pid
            break
        fi
    done
done
